      SUBROUTINE CUTSPALL
!***********************************************************************
!
!  PURPOSE:     Calculates area resulting from cuttings and cavings removed
!               from the intrusion borehole and volume removed by spall.
!               Spall calculations done for all cavities and intrusions;
!               cuttings/cavings area is the same for all cavities and
!               intrusions if the drill diameter is the same.
!
!  AUTHORS:     Harold J. Iuzzolino
!               Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS;
!                              Rewritten from section of code in MAIN;
!                              Removed Model1 and Model2;
!                              Loop on cavity and intrusion (spall only)
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:   None
!
!  Sets AREAS and VOLC in OUTPUT_DATA_MODULE
!
!***********************************************************************

      USE CONSTANTS_MODULE
      USE DEBUG_MODULE
      USE DRILL_PARAMS_MODULE
      USE MODEL_PARAMS_MODULE
      USE OUTPUT_DATA_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER ICAV, IHIT
      INTEGER ODBGX
      DOUBLE PRECISION XDRILDIA, XCOLDIA  !apg REAL
      DOUBLE PRECISION XREPPRES  !apg REAL
      DOUBLE PRECISION XAREAC, XVOLS  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      !..Call DRILL to calculate cuttings and cavings;
      !..Cuttings and cavings is the same for all cavities and intrusions
      !..if the drill diameter is the same

      XAREAC = 0.0
      XCOLDIA = COLDIA
      XDRILDIA = -999.9  !initialize so that DRILL is called

      DO IHIT = 1, NHITS
         DO ICAV = 1, NUMCAVS
            IF (DIAMMOD(IHIT,ICAV,ICURVEC,ICURSCEN) /= XDRILDIA) THEN
               XDRILDIA = DIAMMOD(IHIT,ICAV,ICURVEC,ICURSCEN)
               CALL DRILL (XDRILDIA, XCOLDIA, XAREAC)
            ELSE
               CONTINUE  !XAREAC remains unchanged
            END IF
            AREAC(IHIT,ICAV,ICURVEC,ICURSCEN) = XAREAC
         END DO
      END DO

      !..Calculate spallings, based on requested spall model, for each
      !..cavity and intrusion

      WRITE (ODBG,10000) ISCENID(ICURSCEN), ICURVEC

      !..Initialize spallings
      DO ICAV = 1, NUMCAVS
         DO IHIT = 1, NHITS
            VOLS(IHIT,ICAV,ICURVEC,ICURSCEN) = 0.0
         END DO
      END DO

      WRITE (ODBG,10010) MODTYPE
      IF (MODTYPE /= ' ') THEN
         DO ICAV = 1, NUMCAVS
            DO IHIT = 1, NHITS

               IF ((ICAV == 1) .AND. (IHIT == 1)) THEN
                  ODBGX = ODBG
               ELSE
                  ODBGX = 0
               END IF
               XVOLS = 0.0
               XREPPRES = REPPRES(IHIT,ICAV,ICURVEC,ICURSCEN)

               IF (MODTYPE == 'MODEL3') THEN
                  !..do Model3
                  CALL MODEL3 (ODBGX, XREPPRES, XVOLS)

               ELSE IF (MODTYPE == 'MODEL4') THEN
                  !..do Model4
                  CALL MODEL4 (ODBGX, XREPPRES, XVOLS, ICURVEC)
               END IF

               VOLS(IHIT,ICAV,ICURVEC,ICURSCEN) = XVOLS
            END DO
         END DO
      END IF

      WRITE (ODBG,*)
      WRITE (ODBG,10020)

      DO ICAV = 1, NUMCAVS
         WRITE (ODBG,*)
         DO IHIT = 1, NHITS
            WRITE (ODBG,10030) CAVABBR(ICAV), TIMHIT(IHIT,ICURSCEN), &
               DIAMMOD(IHIT,ICAV,ICURVEC,ICURSCEN), &
               AREAC(IHIT,ICAV,ICURVEC,ICURSCEN), &
               REPPRES(IHIT,ICAV,ICURVEC,ICURSCEN), &
               VOLS(IHIT,ICAV,ICURVEC,ICURSCEN)
         END DO
      END DO

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/, &
         1X,'Cuttings+Cavings and Spallings for Scenario',I2,'  Vector',I4,/)
10010 FORMAT (1X, 'Spall Model: ', A)
10020 FORMAT (1X, 'Cav', '  Time ', &
         ' Drill Diam (m)', '   Cut+Cav AREA', &
         '  Rep Pres (pa)', '   Spall VOLUME')
10030 FORMAT (1X, A3, F7.0, 1P4E15.6)

      END SUBROUTINE CUTSPALL


      SUBROUTINE MODEL3 (ODBGX, REPPRES, VOL)
!***********************************************************************
!
!  PURPOSE:     New model3 for solid blowout.
!               Calculates final blowout radius and waste volume.
!
!  AUTHOR:      A. P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: Cosmetic changes
!               xx-MAY-1997  --APG: First Edition for CUTTINGS
!
!  CALLED BY:   CUTSPALL
!
!  ARGUMENTS:
!     ODBGX     input  Output file, if >0
!     REPPRES   input  Gas pressure in waste pores over repository area (Pa)
!     VOL       output Volume of spallings removed
!
!***********************************************************************

      USE MODEL_PARAMS_MODULE

      IMPLICIT NONE

      INTEGER ODBGX
      DOUBLE PRECISION REPPRES  !apg REAL
      DOUBLE PRECISION VOL  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      IF (ODBGX > 0) THEN
         WRITE (ODBGX,10000) PTHRESH
      END IF

      IF (REPPRES >= PTHRESH) THEN
         VOL = VOLSPALL
      ELSE
         VOL = 0.0
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (1X, 'MODEL3: PTHRESH =', 1PE13.6)

      END SUBROUTINE MODEL3


      SUBROUTINE MODEL4 (ODBGX, REPPRES, VOL, IVEC)
!***********************************************************************
!
!  PURPOSE:     New model4 for solid blowout.
!               Calculates final blowout radius and waste volume.
!
!  AUTHOR:      C. W. Hansen
!
!  UPDATED:     04-APR-2005  --APG: Use vector number as index (if RNDSPALL < 0)
!               xx-SEP-2004  --APG: Cosmetic changes;
!                              Calc ixcdf as (rnd * nvec); was (rnd / (1/nvec))
!               xx-OCT-2003  --CWH: First Edition for CUTTINGS
!
!  CALLED BY:   CUTSPALL
!
!  ARGUMENTS:
!     ODBGX     input  Output file, if >0
!     REPPRES   input  Gas pressure in waste pores over repository area (Pa)
!     VOL       output Volume of spallings removed
!
!***********************************************************************

      USE MODEL_PARAMS_MODULE
      USE MODEL4_DATA_MODULE

      IMPLICIT NONE

      INTEGER ODBGX
      DOUBLE PRECISION REPPRES  !apg REAL
      DOUBLE PRECISION VOL  !apg REAL
      INTEGER IVEC

      DOUBLE PRECISION INTERP  !apg REAL
      INTEGER I, IXCDF

!**** EXECUTABLE STATEMENTS ****

      !..Find index to CDF

      IF (RNDSPALL >= 0.0) THEN
         IXCDF = INT (RNDSPALL * NVEC) + 1
         IF (IXCDF < 1) IXCDF = 1
         IF (IXCDF > NVEC) IXCDF = NVEC
         IF (ODBGX > 0) THEN
            WRITE (ODBGX,10000) RNDSPALL, IXCDF
         END IF
      ELSE
         IXCDF = IVEC
         IF (IXCDF > NVEC) IXCDF = NVEC
      END IF

      !..Find CDFs to interpolate volumes

      IF (REPPRES <= PRES(1)) THEN
         VOL = SPLVOL(IXCDF,1)
      ELSE IF (REPPRES >= PRES(NPRES)) THEN
         VOL = SPLVOL(IXCDF,NPRES)
      ELSE
         I = 1
         DO WHILE (REPPRES > PRES(I))
            I = I + 1
         END DO
         INTERP = (REPPRES - PRES(I-1)) / (PRES(I) - PRES(I-1))
         VOL = SPLVOL(IXCDF,I-1) + &
            INTERP * (SPLVOL(IXCDF,I) - SPLVOL(IXCDF,I-1))
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (1X, 'MODEL4: RNDSPALL =', 1PE13.6, '   Spall Vector', I4)

      END SUBROUTINE MODEL4
