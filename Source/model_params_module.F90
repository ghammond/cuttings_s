      MODULE MODEL_PARAMS_MODULE
!***********************************************************************
!
!  PURPOSE:     Contains model type and model-specific parameters.
!               Also contains repository height and collar diameter.
!
!  UPDATED:     xx-SEP-2004  --APG: Created from BLOWPROP.INC
!
!***********************************************************************

      CHARACTER*8 MODTYPE
      ! MODTYPE = Model type

      !..Model 3 specific inputs:
      DOUBLE PRECISION VOLSPALL, PTHRESH  !apg REAL
      ! VOLSPALL = Sampled volume for spall model 3
      ! PTHRESH  = Pressure threshold for spall model 3

      !..Model 4 specific inputs:
      DOUBLE PRECISION RNDSPALL  !apg REAL
      ! RNDSPALL = Random number for spallings volume CDF

      DOUBLE PRECISION HREPO  !apg REAL
      ! HREPO = Height of repository at burial time (m)

      DOUBLE PRECISION COLDIA  !apg REAL
      ! COLDIA = Collar diameter (m)

      END MODULE MODEL_PARAMS_MODULE
