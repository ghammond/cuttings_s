      SUBROUTINE PREPRO_BRAG (IBRAG)
!***********************************************************************
!
!  PURPOSE:     Processes the input BRAGFLO CAMDAT file.  Reads sizing and
!               arrays that are needed for input file processing.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of BRGCDB
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     IBRAG     input  BRAGFLO CAMDAT file
!
!  Sets BRAG_SIZING_MODULE
!
!***********************************************************************

      USE BRAG_SIZING_MODULE

      IMPLICIT NONE

      INTEGER IBRAG

      INTEGER IERR
      INTEGER IDUM
      INTEGER NQAREC_B, NXINFO_B, &
         NDIM_B, NUMNOD_B, NELX_B, NELY_B, NELZ_B, &
         MAXNENO_B, NUMNPS_B, LNPSNL_B, NUMESS_B, LESSEL_B, LESSNL_B, &
         NVARHI_B, NVARGL_B, NVARND_B
      INTEGER, ALLOCATABLE :: SCRID_B(:)

!**** EXECUTABLE STATEMENTS ****

      !..Read sizing information from BRAGFLO CAMDAT file
      CALL DBISIZES (IBRAG, NQAREC_B, NXINFO_B, NDIM_B, NUMNOD_B, &
         NELBLK_B, NUMEL_B, NELX_B, NELY_B, NELZ_B, &
         MAXNENO_B, NUQATR_B, NUQPRP_B, NUMNPS_B, LNPSNL_B, &
         NUMESS_B, LESSEL_B, LESSNL_B, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT sizing parameters')

      !..Read number of analysis variables from BRAGFLO CAMDAT file
      CALL DBINVAR (IBRAG, NVARHI_B, NVARGL_B, NVARND_B, NVAREL_B, &
         IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading number of BRAGFLO CAMDAT variables')

      !..Reserve dynamic arrays
      ALLOCATE (NAMELB_B(1:NELBLK_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'NAMELB_B', IERR)
      ALLOCATE (NUMLNK_B(1:NELBLK_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'NUMLNK_B', IERR)
      ALLOCATE (NMATPR_B(1:NUQPRP_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'NMATPR_B', IERR)
      ALLOCATE (NAMATR_B(1:NUQATR_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'NAMATR_B', IERR)
      ALLOCATE (NAMEVR_B(1:NVAREL_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'NAMEVR_B', IERR)

      ! Read element block names from BRAGFLO CAMDAT file
      ALLOCATE (SCRID_B(1:NELBLK_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'SCRID_B', IERR)
      CALL DBIELBLK (IBRAG, NELBLK_B, NAMELB_B, SCRID_B, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT material names')
      DEALLOCATE (SCRID_B, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'SCRID_B', IERR)

      !..Read the number of elements per block from BRAGFLO CAMDAT file
      CALL DBINELB (IBRAG, NELBLK_B, NUMLNK_B, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT number of elements per block')

      !..Read property names from BRAGFLO CAMDAT file
      CALL DBIPRNAM (IBRAG, NUQPRP_B, NMATPR_B, IDUM, IDUM, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT property names')

      !..Read attribute names from BRAGFLO CAMDAT file
      CALL DBIATNAM (IBRAG, NUQATR_B, NAMATR_B, IDUM, IDUM, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT attribute names')

      !..Read element variable names from BRAGFLO CAMDAT file
      CALL DBIVRNAM (IBRAG, 'ELEMENT', NVAREL_B, NAMEVR_B, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT element variable names')

      RETURN

      END SUBROUTINE PREPRO_BRAG


      SUBROUTINE CHECK_BRAG (NUMCAVS, IERR)
!***********************************************************************
!
!  PURPOSE:     Checks that the BRAGFLO CAMDAT file contains the items
!               that will be read by READ_BRAG.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     NUMCAVS   input  Number of cavities
!     IERR      output Error flag; returned non-zero if error
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE BRAG_SIZING_MODULE
      USE DEBUG_MODULE

      IMPLICIT NONE

      INTEGER NUMCAVS
      INTEGER IERR

      CHARACTER*8 VARNAM(NBFREQ)

      INTEGER INDX, IVAR, ICAV, IZONE, IX
      LOGICAL ANYREPPRES
      LOGICAL ANYGRIDVOL

! Function definitions
      INTEGER IXCDBNAM

!**** EXECUTABLE STATEMENTS ****

      IERR = 0

      !..Check that required element variables exist on the BRAGFLO CAMDAT file

      VARNAM(1) = NAMPOROSITY
      VARNAM(2) = NAMPRESGAS
      VARNAM(3) = NAMPRESBRIN
      VARNAM(4) = NAMSATGAS
      VARNAM(5) = NAMPERMBRIN

      DO IVAR = 1, NBFREQ
         INDX = IXCDBNAM (VARNAM(IVAR), NVAREL_B, NAMEVR_B)
         IF (INDX <= 0) THEN
            WRITE (ODBG,10000) 'BRAGFLO CAMDAT does not contain ' &
               //'element variable ', VARNAM(IVAR)
            IERR = IERR + 1
         END IF
      END DO

      !..Check that user-specified element variables exist on the
      !..BRAGFLO CAMDAT file

      DO IVAR = 1, NBFVAR
         INDX = IXCDBNAM (NAMBFVAR(IVAR), NVAREL_B, NAMEVR_B)
         IF (INDX <= 0) THEN
            WRITE (ODBG,10000) 'BRAGFLO CAMDAT does not contain ' &
               //'element variable ', NAMBFVAR(IVAR)
            IERR = IERR + 1
         END IF
      END DO

      !..Check that REPPRES element variable exists on the BRAGFLO CAMDAT file,
      !..if needed

      IF (NAMREPPRES == ' ') NAMREPPRES = NAMPRESGAS

      ANYREPPRES = .FALSE.
      DO ICAV = 1, NUMCAVS
         IF (IXZONES_P(ICAV) > 0) ANYREPPRES = .TRUE.
      END DO

      IF (ANYREPPRES) THEN
         INDX = IXCDBNAM (NAMREPPRES, NVAREL_B, NAMEVR_B)
         IF (INDX <= 0) THEN
            WRITE (ODBG,10000) 'BRAGFLO CAMDAT does not contain ' &
               //'element variable ', NAMREPPRES
            IERR = IERR + 1
         END IF
      END IF

      !..Check that GRIDVOL attribute exists on the BRAGFLO CAMDAT file,
      !..if needed

      ANYGRIDVOL = .FALSE.
      DO ICAV = 1, NUMCAVS
         DO IZONE = 1, NZONES(ICAV)
            IX = IXZONES(IZONE,ICAV)
            IF (WTAVG(IX)) ANYGRIDVOL = .TRUE.
         END DO
      END DO
      DO ICAV = 1, NUMCAVS
         IF (IXZONES_P(ICAV) /= 0) THEN
            IX = IXZONES_P(ICAV)
            IF (WTAVG(IX)) ANYGRIDVOL = .TRUE.
         END IF
      END DO

      IF (ANYGRIDVOL) THEN
         INDX = IXCDBNAM (NAMGRIDVOL, NUQATR_B, NAMATR_B)
         IF (INDX <= 0) THEN
            WRITE (ODBG,10000) 'BRAGFLO CAMDAT does not contain ' &
               //'GRIDVOL attribute ', NAMGRIDVOL
            IERR = IERR + 1
         END IF
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/, ' *** ERROR - ', 10A)

      END SUBROUTINE CHECK_BRAG


      SUBROUTINE READ_BRAG (FIBRAG, IBRAG, IERRBRG)
!***********************************************************************
!
!  PURPOSE:     Extracts information from the BRAGFLO CAMDAT file.  The
!               BRAGFLO element variables are processed for all cavities and
!               all intrusions.  The element variables are averaged over
!               specified areas and may be weighted by volume.
!
!  AUTHOR:      Jonathon S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of READBRAG;
!                              Move element block to element numbers to REATXT1;
!                              Data structures to F90;
!                              Process all cavities, as well as intrusions;
!                              Preread CAMDAT steps to only read bounding steps
!               02-FEB-1996  --RAC: Modified to output properties that
!                              BRINE BLOWOUT needs and to be able to
!                              grid volume average on properties from the
!                              BRAGFLO CDB file
!               02-FEB-1996  --RAC: Modified for CUTTINGS, SPALL, TOTALS
!               21-MAR-1994  --JSR: Adapted to new version of CUTTINGS
!               17-FEB-1994  --JSR: Adapted to new version of CUTTINGS
!               05-NOV-1993  --JSR: First Edition for CUTTINGS
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     FIBRAG    input  Filename of BRAGFLO CAMDAT file
!     IBRAG     input  BRAGFLO CAMDAT file
!     IERRBRG   output Error flag; returned non-zero if error
!
!  Sets POROSITY, etc in BRAG_AVERAGE_MODULE
!  Sets DIAMMOD and REPPRES in OUTPUT_DATA_MODULE
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE CONSTANTS_MODULE
      USE DEBUG_MODULE
      USE OUTPUT_DATA_MODULE
      USE PARAM_LOCS_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      CHARACTER*(*) FIBRAG
      INTEGER IBRAG
      INTEGER IERRBRG

      DOUBLE PRECISION, ALLOCATABLE :: GRIDVOL(:)  !apg REAL
      ! GRIDVOL(numel_b) = Grid volumes
      INTEGER, ALLOCATABLE :: HITSTEP(:,:)
      ! HITSTEP(0:nhits,0:1) = Starting (0) and ending (1) time step
      !    corresponding to intrusion time
      CHARACTER*8, ALLOCATABLE :: NAMELB_B(:)
      ! NAMELB_B(nelblk_b) BRAGFLO CAMDAT material names
      INTEGER, ALLOCATABLE :: SCRID(:)
      LOGICAL, ALLOCATABLE :: ISOK(:)
      ! ISOK(nelblk_b) = Logical array for property/attribute
      DOUBLE PRECISION, ALLOCATABLE :: PRPVAL(:)  !apg REAL
      ! PRPVAL(nelblk_b) = Real array for property
      DOUBLE PRECISION, ALLOCATABLE :: VARVAL(:)  !apg REAL
      ! VARVAL(numel_b) = Real array for element variable

      INTEGER NBFALL
      INTEGER MAXBFALL
      PARAMETER (MAXBFALL = NBFREQ + MAXBFVAR)

      CHARACTER*8 VARNAM(MAXBFALL)
      DOUBLE PRECISION EVAVG(MAXALLZONES,MAXBFALL,0:2)  !apg REAL
      DOUBLE PRECISION EVAVG_P(MAXCAVITIES,0:2)  !apg REAL
      ! EVAVG = Average of element variables at (0) old time step,
      !    (1) current time step, (2) interpolated
      DOUBLE PRECISION SUMAVG(MAXALLZONES), SUMAVG_P(MAXCAVITIES)  !apg REAL
      ! SUMAVG = Sum for the average divisor;
      !    if grid-volume average, sum of the grid volume, else element count

      LOGICAL WHOLE
      LOGICAL ANYGRIDVOL
      LOGICAL ANYREPPRES
      INTEGER NQAREC_B, NXINFO_B, &
         NDIM_B, NUMNOD_B, NELBLK_B, NUMEL_B, NELX_B, NELY_B, NELZ_B, &
         MAXNENO_B, NUQATR_B, NUQPRP_B, &
         NUMNPS_B, LNPSNL_B, NUMESS_B, LESSEL_B, LESSNL_B
      INTEGER I, IVAR, ICAV, IZONE, IHIT, IMAT, ISET, ISTEP, IX01, IX, NEL
      INTEGER IERR
      INTEGER IDUM
      DOUBLE PRECISION XDIAMMOD  !apg REAL
      DOUBLE PRECISION TIMBFYR, TIMOLDYR  !apg REAL
      DOUBLE PRECISION TIME  !apg REAL
      DOUBLE PRECISION SVAL  !apg REAL

! Function definitions
      INTEGER IXCDBNAM

!**** EXECUTABLE STATEMENTS ****

      IERRBRG = 0

      WRITE (ODBG,10000) TRIM (FIBRAG)

      IF (NAMREPPRES == ' ') NAMREPPRES = NAMPRESGAS

      VARNAM(1) = NAMPOROSITY
      VARNAM(2) = NAMPRESGAS
      VARNAM(3) = NAMPRESBRIN
      VARNAM(4) = NAMSATGAS
      VARNAM(5) = NAMPERMBRIN
      DO IVAR = 1, NBFVAR
         VARNAM(NBFREQ+IVAR) = NAMBFVAR(IVAR)
      END DO
      NBFALL = NBFREQ + NBFVAR

      !..Read sizing information from BRAGFLO CAMDAT file
      CALL DBISIZES (IBRAG, NQAREC_B, NXINFO_B, NDIM_B, NUMNOD_B, &
         NELBLK_B, NUMEL_B, NELX_B, NELY_B, NELZ_B, &
         MAXNENO_B, NUQATR_B, NUQPRP_B, NUMNPS_B, LNPSNL_B, &
         NUMESS_B, LESSEL_B, LESSNL_B, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Reading BRAGFLO CAMDAT sizing parameters')

!*** Read drill diameter ***

      IF (DIAMMOD_LOC%text == 'BRAGFLO') THEN
         ! Read element block names from BRAGFLO CAMDAT file
         ALLOCATE (SCRID(1:NELBLK_B), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'SCRID', IERR)
         ALLOCATE (NAMELB_B(1:NELBLK_B), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'NAMELB_B', IERR)
         CALL DBIELBLK (IBRAG, NELBLK_B, NAMELB_B, SCRID, IERR)
         IF (IERR /= 0) CALL QAABORT &
            ('Reading BRAGFLO CAMDAT material names')
         DEALLOCATE (SCRID, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'SCRID', IERR)

         !..Read the DIAMMOD property from the BRAGFLO CAMDAT file
         ALLOCATE (ISOK(1:NELBLK_B), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'ISOK', IERR)
         ALLOCATE (PRPVAL(1:NELBLK_B), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'PRPVAL', IERR)
         !..Look for the requested material
         IMAT = IXCDBNAM (DIAMMOD_LOC%mat, NELBLK_B, NAMELB_B)
         IF (IMAT <= 0) THEN
            WRITE (ODBG,10010) 'BRAGFLO CAMDAT does not contain ' &
               //'DIAMMOD material ', DIAMMOD_LOC%mat
            IERRBRG = IERRBRG + 1
         ELSE
            CALL DBIPROP (IBRAG, DIAMMOD_LOC%prp, 0, ISOK, PRPVAL, &
               IDUM, IDUM, IERR)
            IF (IERR /= 0) THEN
               WRITE (ODBG,10010) 'BRAGFLO CAMDAT does not contain ' &
                  //'DIAMMOD property ', DIAMMOD_LOC%prp
               IERRBRG = IERRBRG + 1
            ELSE IF (.NOT. ISOK(IMAT)) THEN
               WRITE (ODBG,10010) 'DIAMMOD property ', DIAMMOD_LOC%prp, &
                  'is not defined for DIAMMOD material ', DIAMMOD_LOC%mat
               IERRBRG = IERRBRG + 1
            ELSE
               XDIAMMOD = PRPVAL(IMAT)
               DO ICAV = 1, NUMCAVS
                  DO IHIT = 1, NHITS
                     DIAMMOD(IHIT,ICAV,ICURVEC,ICURSCEN) = XDIAMMOD
                  END DO
               END DO

               IF (ICURVEC == 1) WRITE (ODBG,10020) &
                  'DIAMMOD read from BRAGFLO CAMDAT material:property ', &
                  DIAMMOD_LOC%mat//':'//DIAMMOD_LOC%prp
               WRITE (ODBG,10050) 'Drill diameter', XDIAMMOD
               WRITE (ODBG,*)
            END IF
         END IF

         DEALLOCATE (ISOK, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'ISOK', IERR)
         DEALLOCATE (PRPVAL, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'PRPVAL', IERR)
         DEALLOCATE (NAMELB_B, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'NAMELB_B', IERR)
      END IF

!*** Read grid volume and sum average divisor ***

      ANYREPPRES = .FALSE.
      DO ICAV = 1, NUMCAVS
         IF (IXZONES_P(ICAV) /= 0) ANYREPPRES = .TRUE.
      END DO

      ANYGRIDVOL = .FALSE.
      DO ICAV = 1, NUMCAVS
         DO IZONE = 1, NZONES(ICAV)
            IX = IXZONES(IZONE,ICAV)
            IF (WTAVG(IX)) ANYGRIDVOL = .TRUE.
         END DO
      END DO
      DO ICAV = 1, NUMCAVS
         IF (IXZONES_P(ICAV) /= 0) THEN
            IX = IXZONES_P(ICAV)
            IF (WTAVG(IX)) ANYGRIDVOL = .TRUE.
         END IF
      END DO

      ALLOCATE (GRIDVOL(1:NUMEL_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'GRIDVOL', IERR)

      !..Read the GRIDVOL attribute for all blocks from the BRAGFLO CAMDAT file
      IF (ANYGRIDVOL) THEN
         ALLOCATE (ISOK(1:NELBLK_B), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'ISOK', IERR)
         CALL DBIATTR (IBRAG, NAMGRIDVOL, 0, ISOK, GRIDVOL, &
            IDUM, IDUM, IERR)
         DEALLOCATE (ISOK, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'ISOK', IERR)
         IF (IERR /= 0) THEN
            WRITE (ODBG,10010) 'BRAGFLO CAMDAT does not contain ' &
               //'GRIDVOL attribute ', NAMGRIDVOL
            IERRBRG = IERRBRG + 1
         ELSE
            IF (ICURVEC == 1) WRITE (ODBG,10020) &
               'GRIDVOL read from BRAGFLO CAMDAT attribute ', NAMGRIDVOL
         END IF
      ELSE
         IF (ICURVEC == 1) WRITE (ODBG,10020) &
            'GRIDVOL is not requested'
      END IF
      IF (ICURVEC == 1) WRITE (ODBG,*)

      !..Calculate a sum over all zones for each area for the average;
      !..if grid-volume, sum the grid volume, else just count elements

      DO ICAV = 1, NUMCAVS
         DO IZONE = 1, NZONES(ICAV)
            IX = IXZONES(IZONE,ICAV)
            SUMAVG(IX) = 0.0
         END DO
      END DO

      IF (ANYREPPRES) THEN
         DO ICAV = 1, NUMCAVS
            IF (IXZONES_P(ICAV) /= 0) THEN
               IX = IXZONES_P(ICAV)
               SUMAVG_P(IX) = 0.0
            END IF
         END DO
      END IF

!*** Determine the BRAGFLO CAMDAT time steps for each intrusion time ***

      ALLOCATE (HITSTEP(0:NHITS,0:1), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'HITSTEP', IERR)

      !..Find the two time steps that bound each intrusion time.
      !..All that is read here is the time; element variables are read later.

      ISTEP = 0
      DO IHIT = 0, NHITS
         HITSTEP(IHIT,0) = ISTEP
         HITSTEP(IHIT,1) = 0

         DO WHILE (.TRUE.)

            !..Advance BRAGFLO CAMDAT to next time step
            ISTEP = ISTEP + 1
            CALL DBISTEP (IBRAG, ISTEP, TIME, WHOLE, IERR)
            IF (IERR /= 0) EXIT
            !..Convert BRAGFLO time in seconds to years
            TIMBFYR = TIME / YR2SEC

            !..Skip if no element variables or TIME < 0
            IF (.NOT. WHOLE) CYCLE
            IF (TIMBFYR < 0.0) CYCLE

            IF (IHIT == 0) THEN
               !..Save the "zero" time step number
               HITSTEP(IHIT,0) = ISTEP
               HITSTEP(IHIT,1) = 0
               EXIT
            ELSE IF (TIMBFYR <= TIMHIT(IHIT,ICURSCEN)) THEN
               !..Save any time step that is less than the intrusion time
               !..(overwritten until time step greater than intrusion time
               !..is found)
               HITSTEP(IHIT,0) = ISTEP
               HITSTEP(IHIT,1) = 0
            ELSE
               !..Save the time step that is greater than the intrusion time,
               !..then go to next intrusion time
               HITSTEP(IHIT,1) = ISTEP
               EXIT
            END IF
         END DO

         IF (IERR /= 0) THEN
            IF (IHIT < NHITS) THEN
               WRITE (ODBG,10010) &
                  'All intrusion times are not on BRAGFLO CAMDAT file'
               IERRBRG = IERRBRG + 1
               RETURN
            END IF
         END IF
      END DO

!*** Calculate the volume-weighted averages ***

      ALLOCATE (VARVAL(1:NUMEL_B), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'VARVAL', IERR)

      !..Read each requested element variable and average the values (with
      !..volume-weighting) over the requested area for each intrusion time.
      !..The variables are usually read from two bounding time steps and
      !..interpolated.

      DO IHIT = 0, NHITS

         !..Read variables and average for the two bounding time steps
         !..for intrusion time

         TIMBFYR = -.999
         DO IX01 = 0, 1
            IF (HITSTEP(IHIT,IX01) >= 1) THEN
               TIMOLDYR = TIMBFYR

               !..Advance BRAGFLO CAMDAT to time step

               CALL DBISTEP (IBRAG, HITSTEP(IHIT,IX01), TIME, WHOLE, &
                  IERR)
               IF (IERR /= 0) THEN
                  WRITE (ODBG,10010) &
                     'Cannot read time step on BRAGFLO CAMDAT'
                  IERRBRG = IERRBRG + 1
                  RETURN
               END IF
               !..Convert BRAGFLO time in seconds to years
               TIMBFYR = TIME / YR2SEC

               !..Read each requested element variable and average the values
               !..over the requested area

               DO IVAR = 1, NBFALL
                  CALL READAVG (IBRAG, VARNAM(IVAR), &
                     MAXZONES, MAXZNSETS, NUMCAVS, NZONES, IXZONES, &
                     NELSETS, IELBEG, IELEND, WTAVG, &
                     GRIDVOL, SUMAVG, &
                     VARVAL, EVAVG(1,IVAR,IX01), IERR)
                  IF (IERR /= 0) THEN
                     WRITE (ODBG,10010) 'BRAGFLO CAMDAT does not contain ' &
                        //'element variable ', VARNAM(IVAR)
                     IERRBRG = IERRBRG + 1
                  END IF
               END DO

               !..Read and average the repository pressure, if needed

               IF (ANYREPPRES) THEN
                  CALL READAVG (IBRAG, NAMREPPRES, &
                     1, MAXZNSETS, NUMCAVS, 1, IXZONES_P, &
                     NELSETS_P, IELBEG_P, IELEND_P, WTAVG_P, &
                     GRIDVOL, SUMAVG_P, &
                     VARVAL, EVAVG_P(1,IX01), IERR)
                  IF (IERR /= 0) THEN
                     WRITE (ODBG,10010) 'BRAGFLO CAMDAT does not contain ' &
                        //'element variable ', NAMREPPRES
                     IERRBRG = IERRBRG + 1
                  END IF
               END IF
            END IF
         END DO

         IF (IHIT == 0) THEN
            WRITE (ODBG,10030) 0.0, TIMBFYR
         ELSE IF (HITSTEP(IHIT,1) < 1) THEN
            WRITE (ODBG,10030) TIMHIT(IHIT,ICURSCEN), TIMBFYR
         ELSE
            WRITE (ODBG,10040) TIMHIT(IHIT,ICURSCEN), TIMOLDYR, TIMBFYR
         END IF

         IF (HITSTEP(IHIT,1) < 1) THEN

            !..Just assign the values for the single time step

            DO ICAV = 1, NUMCAVS
               DO IZONE = 1, NZONES(ICAV)
                  IX = IXZONES(IZONE,ICAV)
                  DO IVAR = 1, NBFALL
                     EVAVG(IX,IVAR,2) = EVAVG(IX,IVAR,0)
                  END DO
               END DO
            END DO

            IF (ANYREPPRES) THEN
               DO ICAV = 1, NUMCAVS
                  IF (IXZONES_P(ICAV) /= 0) THEN
                     IX = IXZONES_P(ICAV)
                     EVAVG_P(IX,2) = EVAVG_P(IX,0)
                  END IF
               END DO
            END IF

         ELSE

            !..Do a linear interpolation between the two bounding steps

            IF (TIMBFYR /= TIMOLDYR) THEN
               SVAL = (TIMHIT(IHIT,ICURSCEN) - TIMOLDYR) / (TIMBFYR - TIMOLDYR)
            ELSE
               SVAL = 0.0
            END IF

            DO ICAV = 1, NUMCAVS
               DO IZONE = 1, NZONES(ICAV)
                  IX = IXZONES(IZONE,ICAV)
                  DO IVAR = 1, NBFALL
                     EVAVG(IX,IVAR,2) = &
                        SVAL * (EVAVG(IX,IVAR,1) - EVAVG(IX,IVAR,0)) &
                        + EVAVG(IX,IVAR,0)
                  END DO
               END DO
            END DO

            IF (ANYREPPRES) THEN
               DO ICAV = 1, NUMCAVS
                  IF (IXZONES_P(ICAV) /= 0) THEN
                     IX = IXZONES_P(ICAV)
                     EVAVG_P(IX,2) = &
                        SVAL * (EVAVG_P(IX,1) - EVAVG_P(IX,0)) &
                        + EVAVG_P(IX,0)
                  END IF
               END DO
            END IF
         END IF

         IF (IHIT == 0) THEN

            !..For time 0, just assign the values for porosity

            DO ICAV = 1, NUMCAVS
               DO IZONE = 1, NZONES(ICAV)
                  IX = IXZONES(IZONE,ICAV)
                  POROSITY(IZONE,IHIT,ICAV) = EVAVG(IX,1,2)
               END DO
            END DO

         ELSE

            !..Assign the values for all variables

            DO ICAV = 1, NUMCAVS
               DO IZONE = 1, NZONES(ICAV)
                  IX = IXZONES(IZONE,ICAV)
                  POROSITY(IZONE,IHIT,ICAV) = EVAVG(IX,1,2)
                  PRESGAS(IZONE,IHIT,ICAV)  = EVAVG(IX,2,2)
                  PRESBRIN(IZONE,IHIT,ICAV) = EVAVG(IX,3,2)
                  SATGAS(IZONE,IHIT,ICAV)   = EVAVG(IX,4,2)
                  PERMBRIN(IZONE,IHIT,ICAV) = EVAVG(IX,5,2)
                  DO IVAR = 1, NBFVAR
                     VALBFVAR(IVAR,IZONE,IHIT,ICAV) = EVAVG(IX,NBFREQ+IVAR,2)
                  END DO
               END DO
            END DO

            !..Assign the values for repository pressure

            IF (ANYREPPRES) THEN
               DO ICAV = 1, NUMCAVS
                  IF (IXZONES_P(ICAV) /= 0) THEN
                     IX = IXZONES_P(ICAV)
                     REPPRES(IHIT,ICAV,ICURVEC,ICURSCEN) = EVAVG_P(IX,2)
                  END IF
               END DO
            END IF
         END IF
      END DO

      !..Assign PRESGAS to REPPRES if no zones specified

      DO ICAV = 1, NUMCAVS
         IF (IXZONES_P(ICAV) == 0) THEN
            DO IHIT = 1, NHITS
               REPPRES(IHIT,ICAV,ICURVEC,ICURSCEN) = PRESGAS(1,IHIT,ICAV)
            END DO
         END IF
      END DO

      DEALLOCATE (HITSTEP, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'HITSTEP', IERR)
      DEALLOCATE (VARVAL, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'VARVAL', IERR)
      DEALLOCATE (GRIDVOL, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'GRIDVOL', IERR)

      !*** Print results to output debug file ***

      IF (ICURVEC == 1) THEN
         WRITE (ODBG,*)
         WRITE (ODBG,10060) &
            NAMPOROSITY, &
            NAMPRESGAS, &
            NAMPRESBRIN, &
            NAMSATGAS, &
            NAMPERMBRIN, &
            (NAMBFVAR(IVAR), IVAR=1,NBFVAR)

         DO ICAV = 1, NUMCAVS
            DO IHIT = 1, NHITS
               WRITE (ODBG,*)
               DO IZONE = 1, NZONES(ICAV)
                  WRITE (ODBG,10070) &
                     CAVABBR(ICAV), TIMHIT(IHIT,ICURSCEN), IZONE-1, &
                     POROSITY(IZONE,IHIT,ICAV), &
                     PRESGAS(IZONE,IHIT,ICAV), &
                     PRESBRIN(IZONE,IHIT,ICAV), &
                     SATGAS(IZONE,IHIT,ICAV), &
                     PERMBRIN(IZONE,IHIT,ICAV), &
                     (VALBFVAR(IVAR,IZONE,IHIT,ICAV), IVAR=1,NBFVAR)
               END DO
            END DO
         END DO
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/, &
         1X,'Reading BRAGFLO CAMDAT file',/,4X,A,/)
10010 FORMAT (' *** ERROR - ', 10A)
10020 FORMAT (1X, 10A)
10030 FORMAT (1X, 'Intrusion Time =', F7.0, &
         3X, 'BRAGFLO Time ', 10X, F10.2, ' years')
10040 FORMAT (1X, 'Intrusion Time =', F7.0, &
         3X, 'BRAGFLO Times', 2F10.2, ' years', 1PE10.2)
10050 FORMAT (1X, A, ' =', 1PE13.5)
10060 FORMAT (1X, 'Cav', '  Time ', ' Zone', 99(3X,A8,2X))
10070 FORMAT (1X, A3, F7.0, I4, 1P99E13.5)

      END SUBROUTINE READ_BRAG


      SUBROUTINE READAVG (IBRAG, VARNAM, &
         MAXZONES, MAXZNSETS, NUMCAVS, NZONES, IXZONES, &
         NELSETS, IELBEG, IELEND, WTAVG, &
         GRIDVOL, SUMAVG, &
         VARVAL, EVAVG, IERR)
!***********************************************************************
!
!  PURPOSE:     Reads variable and calculates average, which may be weighted
!               by grid volume.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS
!                              (adapted from portions of READ_BRAG)
!
!  CALLED BY:   READ_BRAG
!
!  ARGUMENTS:
!     IBRAG     input  BRAGFLO CAMDAT file
!     VARNAM    input  Name of the element variable to be read
!     MAXZONES, MAXZNSETS, NUMCAVS, NZONES, IXZONES,
!        NELSETS, IELBEG, IELEND, WTAVG
!               input  Zone identifiers from BRAG_AVERAGE_MODULE; may be
!                      repository pressure zones
!     GRIDVOL   input  Grid volume
!     SUMAVG    in/out Average divisor (volume sum or number of elements);
!                      will be set if zero
!     VARVAL    output Values of the element variable
!     EVAVG     output Volume-weighted average
!     IERR      output Returned non-zero if error
!
!***********************************************************************

      INTEGER IBRAG
      CHARACTER*8 VARNAM
      DOUBLE PRECISION VARVAL(*)  !apg REAL
      INTEGER MAXZONES
      INTEGER MAXZNSETS
      INTEGER NUMCAVS
      INTEGER NZONES(NUMCAVS)
      INTEGER IXZONES(MAXZONES,NUMCAVS)
      INTEGER NELSETS(*)
      INTEGER IELBEG(MAXZNSETS,*), IELEND(MAXZNSETS,*)
      LOGICAL WTAVG(*)
      DOUBLE PRECISION GRIDVOL(*)  !apg REAL
      DOUBLE PRECISION SUMAVG(*)  !apg REAL
      DOUBLE PRECISION EVAVG(*)  !apg REAL
      INTEGER IERR

      INTEGER ICAV, IZONE, ISET, I, IX, NR
      INTEGER IDUM
      DOUBLE PRECISION SUM  !apg REAL

      !..Read the values of the requested element variable for the
      !..current time step from the BRAGFLO CAMDAT file
      CALL DBIVAR (IBRAG, 'ELEMENT', VARNAM, 0, VARVAL, &
         IDUM, IDUM, IERR)
      IF (IERR /= 0) RETURN

      !..Calculate a sum over all zones for each area for the average;
      !..if grid-volume, sum the grid volume, else just count elements

      DO ICAV = 1, NUMCAVS
         IF (MAXZONES <= 1) NR = MAXZONES
         IF (MAXZONES > 1) NR = NZONES(ICAV)
         DO IZONE = 1, NR
            IX = IXZONES(IZONE,ICAV)
            IF (SUMAVG(IX) == 0.0) THEN
               IF (WTAVG(IX)) THEN
                  !..Sum the grid volume over all zones for each area
                  SUM = 0.0
                  DO ISET = 1, NELSETS(IX)
                     DO I = IELBEG(ISET,IX), IELEND(ISET,IX)
                        SUM = SUM + GRIDVOL(I)
                     END DO
                  END DO
                  SUMAVG(IX) = SUM
               ELSE
                  !..Count the number of elements to average over for each area
                  NEL = 0
                  DO ISET = 1, NELSETS(IX)
                     NEL = NEL + IELEND(ISET,IX) - IELBEG(ISET,IX) + 1
                  END DO
                  SUMAVG(IX) = NEL
               END IF
            END IF
         END DO
      END DO

      !..Calculate the average (may include grid volume) over requested areas

      DO ICAV = 1, NUMCAVS
         IF (MAXZONES <= 1) NR = MAXZONES
         IF (MAXZONES > 1) NR = NZONES(ICAV)
         DO IZONE = 1, NR
            IX = IXZONES(IZONE,ICAV)
            SUM = 0.0
            DO ISET = 1, NELSETS(IX)
               DO I = IELBEG(ISET,IX), IELEND(ISET,IX)
                  IF (WTAVG(IX)) THEN
                     SUM = SUM + VARVAL(I) * GRIDVOL(I)
                  ELSE
                     SUM = SUM + VARVAL(I)
                  END IF
               END DO
            END DO
            EVAVG(IX) = SUM / SUMAVG(IX)
         END DO
      END DO

      RETURN
      END SUBROUTINE READAVG


      SUBROUTINE FINALPORO
!***********************************************************************
!
!  PURPOSE:     Calculates final porosity and final height for all cavities
!               and intrusions
!
!  AUTHORS:     Harold J. Iuzzolino
!               Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS;
!                              created from section of code in MAIN
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:   None
!
!  Sets POROF and HFINAL in BRAG_AVERAGE_MODULE
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE DEBUG_MODULE
      USE MODEL_PARAMS_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER ICAV, IHIT, IZONE
      DOUBLE PRECISION XPOROB, XPOROF, XPOROI, XHF  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      !..Calculate final porosity and final height

      IF (BRAGCDB) THEN
         IF (ICURVEC == 1) THEN
            WRITE (ODBG,10000)
10000       FORMAT ( &
               /,1X,'From these two relationships:' &
               /,1X,'   (1.0-Pi)*Hi = (1.0-Pf)*Hf'  &
               /,1X,'         Pf*Hf = Pb*Hi'        &
               /,1X,'Where:'                        &
               /,1X,'   Pi = Initial porosity'      &
               /,1X,'   Hi = Initial height'        &
               /,1X,'   Pb = BRAGFLO porosity'      &
               /,1X,'   Pf =  Final  porosity'      &
               /,1X,'   Hf =  Final  height'        &
               /,1X,'Code will calculate:'          &
               /,1X,'   Pf = Pb/(1.0-Pi+Pb)'        &
               /,1X,'   Hf = (1.0-Pi)/(1.0-Pf)*Hi'  )
         END IF
      END IF

      DO ICAV = 1, NUMCAVS
         DO IHIT = 1, NHITS
            DO IZONE = 1, NZONES(ICAV)
               XPOROB = POROSITY(IZONE,IHIT,ICAV)
               XPOROI = POROSITY(IZONE,0,ICAV)

               IF (BRAGCDB) THEN
                  XPOROF = XPOROB / (1.0 - XPOROI + XPOROB)
               ELSE
                  XPOROF = POROSITY(IZONE,IHIT,ICAV)
               END IF
               POROF(IZONE,IHIT,ICAV) = XPOROF

               XHF = (1.0 - XPOROI) / (1.0 - XPOROF) * HREPO
               HFINAL(IZONE,IHIT,ICAV) = XHF
            END DO
         END DO
      END DO

      IF (BRAGCDB) THEN
         IF (ICURVEC == 1) THEN
            WRITE (ODBG,*)
            WRITE (ODBG,10010) 'HREPO (initial repository height)', HREPO
            WRITE (ODBG,*)
            WRITE (ODBG,10020) &
               'InitPoro', &
               'BRAGPoro', &
               'POROF   ', &
               'HFINAL  '

            DO ICAV = 1, NUMCAVS
               DO IHIT = 1, NHITS
                  WRITE (ODBG,*)
                  DO IZONE = 1, NZONES(ICAV)
                     WRITE (ODBG,10030) &
                        CAVABBR(ICAV), TIMHIT(IHIT,ICURSCEN), IZONE-1, &
                        POROSITY(IZONE,0,ICAV), &
                        POROSITY(IZONE,IHIT,ICAV), &
                        POROF(IZONE,IHIT,ICAV), &
                        HFINAL(IZONE,IHIT,ICAV)
                  END DO
               END DO
            END DO
         END IF
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10010 FORMAT (1X, A, ' =', 1PE13.5)
10020 FORMAT (1X, 'Cav', '  Time ', ' Zone', 99(3X,A8,2X))
10030 FORMAT (1X, A3, F7.0, I4, 1P99E13.5)

      END SUBROUTINE FINALPORO


      SUBROUTINE WRITE_CAMDATS (ICDB, FOCDB, OCDB)
!***********************************************************************
!
!  PURPOSE:     Writes cuttings results and BRAGFLO averages for all cavities
!               and intrusions to output CAMDAT files.  One CAMDAT file
!               for each cavity and intrusion.
!
!  AUTHORS:     Harold J. Iuzzolino
!               Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of GENCUT;
!                              Remove all radionuclide calcs;
!                              Do all CAMDAT operations (open, header,
!                              write data, close); Loop on IHIT
!               02-FEB-1996  --RAC: Added capability to process curies
!                                   for CUTTINGS/CRAVING alone,
!                                   for SPALL alone,
!                                   for TOTAL
!               18-MAR-1994  --JSR: Fixed computation of SUMCI(*)
!                                   (imaginary HIT#0 was being added)
!               23-FEB-1994  --JSR: Fixed calls to QUANKG such that
!                                   XMOLES(*) array is constant.
!               21-FEB-1994  --JSR: Adapted to new HJIUZZO radioisotope
!                                   decay chain algorithm based on
!                                   simplified solution of the
!                                   Bateman's equations, etc...
!               18-JAN-1994  --JSR: Corrected XNORM(*) for
!                                   "NORMALIZATION" w.r.t.
!                                   EPA 40 CFR 191 $13 standard.
!               06-DEC-1993  --JSR: Added STABLE array in arg. list.
!               11-NOV-1993  --JSR: Completely NEW version
!               29-JUL-1992  --JSR: Deleted unused variables
!               17-JUL-1991  --JWB: WRITE STATEMENTS ADDED
!               04-JAN-1991  --JSR: Write to file NOUTFL
!               19-DEC-1990  --JSR: Deleted writing to UNIT 2;
!                                   Added arguments in call
!               13-SEP-1990  --HJI: EXTENDED OPTIONS TO INCLUDE
!                                   MULTIPLE HITS
!               02-JUN-1990  --HJI: INVENTORY DISTRIBUTED OVER
!                                   109,380 sq meter
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     ICDB      input  Input CAMDAT file (connected to output CAMDAT file)
!     FOCDB     input  Filename of output CAMDAT file
!     OCDB      input  Unit number for output CAMDAT file
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE DEBUG_MODULE
      USE OUTPUT_DATA_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER ICDB
      CHARACTER*(*) FOCDB
      INTEGER OCDB

      INTEGER ICAV, IHIT
      INTEGER IDUM, IERR
      CHARACTER*132 FILNAM

!**** EXECUTABLE STATEMENTS ****

      WRITE (ODBG,10000)

      !..Loop over all cavities
      DO ICAV = 1, NUMCAVS

         !..Loop over all intrusions
         DO IHIT = 1, NHITS

            !..Open output CAMDAT file (connected to input CAMDAT file)
!apg        !..Expand with lowercase cavity, if given
            CALL EXPAND_TEMPLATE (FOCDB, .FALSE., &
               ISCENID(ICURSCEN), ICURVEC, CAVABBRl(ICAV), &
               NINT(TIMHIT(IHIT,ICURSCEN)), &
               FILNAM)
            WRITE (ODBG,10010) TRIM (FILNAM)
            CALL DBOOPEN (OCDB, ICDB, IDUM, FILNAM, IERR)
            IF (IERR /= 0) CALL QAABORT &
               ('Opening output CAMDAT file')

            !..Write output CAMDAT file header section
            CALL WRITE_CDBHEAD (OCDB)

            !..Write cuttings results and BRAGFLO averages to CAMDAT file
            CALL WRITE_CDBVARS (OCDB, .TRUE., ICAV, IHIT)

            CALL DBOCLOSE (OCDB, IERR)
            IF (IERR /= 0) CALL QAABORT &
               ('Closing output CAMDAT file')
         END DO
      END DO

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/, &
         1X,'Writing CAMDAT files',/)
10010 FORMAT (4X, A)

      END SUBROUTINE WRITE_CAMDATS


      SUBROUTINE WRITE_CDBHEAD (OCDB)
!***********************************************************************
!
!  PURPOSE:     Writes the "header" section of CAMDAT to the output CAMDAT
!               file.  The CUTTINGS parameters are output as properties.
!
!  AUTHOR:      Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of WRCHEA;
!                              Make it easier to add new properties;
!                              Move the write of global variable names into
!                              WRITE_CDBVARS
!               29-FEB-2000  --APG: Fixed the way properties are added
!                              in response to CAMDAT_LIB change
!               02-FEB-1996  --RAC: Added output properties by
!                              brag brine flow problem & curies
!                              brought to surface by CUTTINGS,
!                              SPALL, & TOTAL
!               21-MAR-1994  --Added SAVNAM array in arg. list
!               17-FEB-1994  --Adapted for DIAMMOD(*), AINTC(*)
!                              arrays to be written to CAMDAT as
!                              global variables, etc...
!               04-NOV-1993  --Completely new
!               07-AUG-1992  --Added error statements
!               28-JUL-1992  --First Ed. for CUTTINGS using CAMDAT_LIB
!
!  CALLED BY:   WRITE_CAMDATS
!
!  ARGUMENTS:
!     OCDB      output Output CAMDAT file
!
!***********************************************************************

      USE CDB_PROPS_MODULE
      USE DRILL_PARAMS_MODULE

      IMPLICIT NONE

      INTEGER OCDB

      LOGICAL LDUM
      INTEGER I, IDUM, IERR
      INTEGER IXOLD
      INTEGER IOMAT
      DOUBLE PRECISION RDUM  !apg REAL
      CHARACTER*8 QAREC(4,1)

      LOGICAL, ALLOCATABLE :: ISOK(:)
      DOUBLE PRECISION, ALLOCATABLE :: PRPVAL(:)  !apg REAL
      ! ISOK(nelblk) = Logical array for property
      ! PRPVAL(nelblk) = Real array for property

! These variables are for the properties to be added; max of 20
      INTEGER IPRX
      INTEGER NUMPRX
      CHARACTER*8 NAMPRX(20)
      DOUBLE PRECISION VALPRX(20)  !apg REAL

! Function definitions
      INTEGER IXCDBNAM

!**** EXECUTABLE STATEMENTS ****

      !..Add new QA records to CAMDAT file

      I = 0
      CALL QAMAKREC (I, QAREC)
      CALL DBOQAREC (OCDB, 'ADD', QAREC, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Adding QA record to CAMDAT file')

      !..Set up the names and values of the parameters to be written
      NUMPRX = 0
      NUMPRX = NUMPRX + 1
      NAMPRX(NUMPRX) = 'DENSITY '
      VALPRX(NUMPRX) = DENSITY
      NUMPRX = NUMPRX + 1
      NAMPRX(NUMPRX) = 'DOMEGA  '
      VALPRX(NUMPRX) = DOMEGA
      NUMPRX = NUMPRX + 1
      NAMPRX(NUMPRX) = 'ABSRO   '
      VALPRX(NUMPRX) = ABSRO
      NUMPRX = NUMPRX + 1
      NAMPRX(NUMPRX) = 'TAUFAIL '
      VALPRX(NUMPRX) = TAUFAIL
      NUMPRX = NUMPRX + 1
      NAMPRX(NUMPRX) = 'VISCO   '
      VALPRX(NUMPRX) = VISCO
      NUMPRX = NUMPRX + 1
      NAMPRX(NUMPRX) = 'YLDSTRSS'
      VALPRX(NUMPRX) = YLDSTRSS

      !..Find the material where output variables are to be written
      IOMAT = IXCDBNAM (OUTMAT, NELBLK, NAMELB)
      IF (IOMAT <= 0) IOMAT = NELBLK

      ALLOCATE (PRPVAL(1:NELBLK), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'PRPVAL', IERR)
      ALLOCATE (ISOK(1:NELBLK), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'ISOK', IERR)

      !..Set up an empty property to be filled in for new property
      DO I = 1, NELBLK
         PRPVAL(I) = 0.0
         ISOK(I) = .FALSE.
      END DO

      !..Add each parameter as a CAMDAT property (may already exist)

      DO IPRX = 1, NUMPRX
         !..Find the property name, if it exists
         IXOLD = IXCDBNAM (NAMPRX(IPRX), NUQPRP, NMATPR)
         IF (IXOLD <= 0) THEN
            !..If new property, add it with new value
            PRPVAL(IOMAT) = VALPRX(IPRX)
            ISOK(IOMAT) = .TRUE.
            CALL DBOPROP (OCDB, 'ADD', NAMPRX(IPRX), ISOK, PRPVAL, IERR)
         ELSE
            !..If existing property, delete it and replace it with new value
            CALL DBOPROP (OCDB, 'DELETE', NAMPRX(IPRX), LDUM, RDUM, IERR)
            XMATPR(IOMAT,IXOLD) = VALPRX(IPRX)
            IASPRP(IOMAT,IXOLD) = .TRUE.
            CALL DBOPROP (OCDB, 'ADD', NAMPRX(IPRX), &
               IASPRP(1,IXOLD), XMATPR(1,IXOLD), IERR)
         END IF
         IF (IERR /= 0) CALL QAABORT &
            ('Writing CAMDAT property '//NAMPRX(IPRX))
      END DO

      DEALLOCATE (PRPVAL, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'PRPVAL', IERR)
      DEALLOCATE (ISOK, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'ISOK', IERR)

      !..Write all "header" information to the CAMDAT file

      CALL DBOHEAD (OCDB, IDUM, IDUM, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Writing "header" to the CAMDAT file')

      RETURN

      END SUBROUTINE WRITE_CDBHEAD


      SUBROUTINE WRITE_CDBVARS (OCDB, FIRSTSTEP, ICAV, IHIT)
!***********************************************************************
!
!  PURPOSE:     Writes analysis section of CAMDAT for a single timestep.
!               Results are output as global variables.  For the first
!               step only, the variable names are written to the CAMDAT file.
!
!  AUTHOR:      Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of WRTANA
!                              Make it easier to add/rename variables;
!                              Variables names written on first step
!               02-FEB-1996  --RAC: Modified for CUTTINGS, SPALL, TOTALS
!               21-MAR-1994  --JSR: Adapted to new version of CUTTINGS
!               17-FEB-1994  --JSR: Adapted to new version of CUTTINGS
!               05-NOV-1993  --JSR: First Edition for CUTTINGS
!
!  CALLED BY:   WRITE_CAMDATS
!
!  ARGUMENTS:
!     OCDB      output Output CAMDAT file
!     FIRSTSTEP input  True iff first time step on CAMDAT file
!     ICAV      input  Cavity index
!     IHIT      input  Intrusion index
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE CONSTANTS_MODULE
      USE MODEL_PARAMS_MODULE
      USE OUTPUT_DATA_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER OCDB
      LOGICAL FIRSTSTEP
      INTEGER ICAV
      INTEGER IHIT

      INTEGER I, IDUM, IERR, IGV, IVAR, IZONE
      LOGICAL LDUM
      LOGICAL WHOLE
      DOUBLE PRECISION TIMES  !apg REAL
      DOUBLE PRECISION XSATBRIN  !apg REAL
      CHARACTER*8 NAMTMP

! A maximum of 200 global variables may be written
      INTEGER NUMGV
      CHARACTER*8 NAMGV(200)
      DOUBLE PRECISION VALGV(200)  !apg REAL

      CHARACTER*1 CZONE(10)
      DATA CZONE /'0','1','2','3','4','5','6','7','8','9'/

!**** EXECUTABLE STATEMENTS ****

      NUMGV = 0

      !..Area from CUTTINGS
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'AREA_C  '
      VALGV(NUMGV) = AREAC(IHIT,ICAV,ICURVEC,ICURSCEN)

      !..Area from SPALL
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'AREA_S  '
      VALGV(NUMGV) = VOLS(IHIT,ICAV,ICURVEC,ICURSCEN) / HREPO

      !..Total area CUTTINGS + SPALL
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'AREA_T  '
      VALGV(NUMGV) = AREAC(IHIT,ICAV,ICURVEC,ICURSCEN) &
         + (VOLS(IHIT,ICAV,ICURVEC,ICURSCEN) / HREPO)

      !..Volume from CUTTINGS
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'VOL_C   '
      VALGV(NUMGV) = AREAC(IHIT,ICAV,ICURVEC,ICURSCEN) * HREPO

      !..Volume from SPALL
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'VOL_S   '
      VALGV(NUMGV) = VOLS(IHIT,ICAV,ICURVEC,ICURSCEN)

      !..Total Volume CUTTINGS + SPALL
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'VOL_T   '
      VALGV(NUMGV) = (AREAC(IHIT,ICAV,ICURVEC,ICURSCEN) * HREPO) &
         + VOLS(IHIT,ICAV,ICURVEC,ICURSCEN)

      !..Drill Bit Diameter used in this intrusion
      NUMGV = NUMGV + 1
      NAMGV(NUMGV) = 'DRILDIAM'
      VALGV(NUMGV) = DIAMMOD(IHIT,ICAV,ICURVEC,ICURSCEN)

      IF (BRAGCDB) THEN

         !..Multiple drilling hits from BRAGFLO CAMDAT file
         NUMGV = NUMGV + 1
         NAMGV(NUMGV) = 'NUM_INTR'
         VALGV(NUMGV) = NZONES(ICAV)

         DO IZONE = 1, NZONES(ICAV)

            !..Calculated final porosity from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = NAMPOROSITY
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            VALGV(NUMGV) = POROF(IZONE,IHIT,ICAV)

            !..Calculated final height from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = 'HFINAL_'
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            VALGV(NUMGV) = HFINAL(IZONE,IHIT,ICAV)

            !..Gas pressure from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = NAMPRESGAS
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            VALGV(NUMGV) = PRESGAS(IZONE,IHIT,ICAV)

            !..Brine pressure from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = NAMPRESBRIN
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            VALGV(NUMGV) = PRESBRIN(IZONE,IHIT,ICAV)

            !..Saturation of gas from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = NAMSATGAS
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            VALGV(NUMGV) = SATGAS(IZONE,IHIT,ICAV)

            !..Saturation of brine from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = NAMSATBRIN
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            XSATBRIN = 1.0 - SATGAS(IZONE,IHIT,ICAV)
            VALGV(NUMGV) = XSATBRIN

            !..Permeability of brine from BRAGFLO CAMDAT file
            NUMGV = NUMGV + 1
            NAMTMP = NAMPERMBRIN
            I = LEN_TRIM (NAMTMP)
            IF (I >= 8) I = 8-1
            NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
            VALGV(NUMGV) = PERMBRIN(IZONE,IHIT,ICAV)

            !..User-specified variables from BRAGFLO CAMDAT file
            DO IVAR = 1, NBFVAR
               NUMGV = NUMGV + 1
               NAMTMP = NAMBFVAR(IVAR)
               I = LEN_TRIM (NAMTMP)
               IF (I >= 8) I = 8-1
               NAMGV(NUMGV) = NAMTMP(1:I)//CZONE(IZONE)
               VALGV(NUMGV) = VALBFVAR(IVAR,IZONE,IHIT,ICAV)
            END DO
         END DO

      ELSE

         !..Calculated final porosity from input control file porosity
         NUMGV = NUMGV + 1
         NAMGV(NUMGV) = 'POROSITY'
         VALGV(NUMGV) = POROF(1,IHIT,ICAV)

         !..Calculated final height from input control file porosity
         NUMGV = NUMGV + 1
         NAMGV(NUMGV) = 'HFINAL  '
         VALGV(NUMGV) = HFINAL(1,IHIT,ICAV)

         !..Gas pressure from input control file
         NUMGV = NUMGV + 1
         NAMGV(NUMGV) = 'PRESGAS '
         VALGV(NUMGV) = PRESGAS(1,IHIT,ICAV)
      END IF

      !..(FIRST STEP ONLY) Write the global variable names to the CAMDAT file

      IF (FIRSTSTEP) THEN
         DO IGV = 1, NUMGV
            CALL DBOVRNAM (OCDB, 'GLOBAL', 'ADD', 1, NAMGV(IGV), &
               LDUM, IDUM, IDUM, IERR)
            IF (IERR /= 0) CALL QAABORT &
               ('Adding CAMDAT global variable name '//NAMGV(IGV))
         END DO
      END IF

      !..Write the time step time to the CAMDAT file

      WHOLE = .TRUE.

      !..Convert time to seconds
      TIMES = TIMHIT(IHIT,ICURSCEN) * YR2SEC
      CALL DBOTIME (OCDB, TIMES, WHOLE, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Writing CAMDAT time step time')

      !..Write the global variables to the CAMDAT file

      DO IGV = 1, NUMGV
         CALL DBOVAR (OCDB, 'GLOBAL', NAMGV(IGV), 0, VALGV(IGV), &
            IDUM, IDUM, IERR)
         IF (IERR /= 0) CALL QAABORT &
            ('Writing CAMDAT global variable '//NAMGV(IGV))
      END DO

      !..Write the completed time-step and advance to the next step

      CALL DBOSTEP (OCDB, IDUM, IDUM, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Writing time step (DBOSTEP)')

      RETURN

      END SUBROUTINE WRITE_CDBVARS
