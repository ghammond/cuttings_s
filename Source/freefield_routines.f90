!***********************************************************************
!
!  PURPOSE:     These routines interact with CAMCON_LIB free-field reader
!               to read a text file line-by-line and return fields,
!               as requested.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: Adapted existing routines (which included
!                              free-field reader) to use CAMCON_LIB FFRDFLDS
!
!  CALLED BY:   input routines
!
!***********************************************************************

      SUBROUTINE NEXTLINE (IN0, ILOG0, IOSTAT)
!***********************************************************************
!
!  PURPOSE:     Reads the next line with CAMSUPES_LIB free-field reader.
!               NEXTLINE must be called before the routines that return
!               individual fields (NEXTWORD, NEXT_C, etc.).
!
!  ARGUMENTS:
!     INO       input  Unit number of the input text file to read
!     ILOG0     input  Unit number of the log file for error messages
!                      and echoed input; if <=0, no output
!     IOSTAT    output I/O status of the input line; <0 if end of file
!
!***********************************************************************

      USE FREEFIELD_MODULE

      IMPLICIT NONE

      INTEGER IN0, ILOG0
      INTEGER IOSTAT

      ILOG = ILOG0

  100 CONTINUE
      CALL FFRDFLDS (IN0, ILOG0, '', MAXFLDS, IOSTAT, &
         NUMFLDS, ITYPFLDS, CFLDS, IFLDS, RFLDS)
      IF (IOSTAT /= 0) THEN
         NUMFLDS = 1
         ITYPFLDS(1) = 0
         CFLDS(1) = 'EOF'
         RETURN
      END IF
      IF (NUMFLDS <= 0) GOTO 100

      IFLD = 0
      IF (CFLDS(NUMFLDS) == '<') THEN
         NUMFLDS = NUMFLDS - 1
      END IF

      RETURN
      END


      SUBROUTINE NEXTWORD (KTYPE, COUT, IOUT, ROUT)
!***********************************************************************
!
!  PURPOSE:     Returns the next field from line read by NEXTLINE.
!
!  ARGUMENTS:
!     KTYPE     output Returned type of the free-field read field processed:
!                       <0 = no more fields
!                        0 = character
!                        1 = real
!                        2 = integer
!     COUT      output Returned character field
!     IOUT      output Returned integer field
!     ROUT      output Returned real field
!
!***********************************************************************

      USE FREEFIELD_MODULE

      IMPLICIT NONE

      INTEGER KTYPE
      CHARACTER*(*) COUT
      INTEGER IOUT
      DOUBLE PRECISION ROUT  !apg REAL

      IF (IFLD < NUMFLDS) THEN
         IFLD = IFLD + 1
         KTYPE = ITYPFLDS(IFLD)
         COUT = CFLDS(IFLD)
         IOUT = IFLDS(IFLD)
         ROUT = RFLDS(IFLD)

      ELSE
         KTYPE = -999
         COUT = ' '
         IOUT = 0
         ROUT = 0.0
      END IF

      RETURN
      END


      SUBROUTINE NEXT_C (COUT, IERR)
!***********************************************************************
!
!  PURPOSE:     Returns the next field from line.  If there is no field,
!               an error results.  (Any field type is returned as character.)
!
!  ARGUMENTS:
!     COUT      output Returned character field
!     IERR      output Returned non-zero if there is no field
!
!***********************************************************************

      USE FREEFIELD_MODULE

      IMPLICIT NONE

      CHARACTER*(*) COUT
      INTEGER IERR

      INTEGER KTYPE

      IF (IFLD < NUMFLDS) THEN
         IFLD = IFLD + 1
         KTYPE = ITYPFLDS(IFLD)
         IF (KTYPE == 0) THEN
            COUT = CFLDS(IFLD)
         ELSE
            IERR = IERR + 1
            IF (ILOG > 0) WRITE (ILOG,10000) CFLDS(IFLD)
         END IF

      ELSE
         KTYPE = -999
         COUT  = ' '
         IERR = IERR + 1
         IF (ILOG > 0) WRITE (ILOG,10000) 'EOF'
      END IF

      RETURN
10000 FORMAT (' *** String expected, not ', A)
      END


      SUBROUTINE NEXT_I (IOUT, IERR)
!***********************************************************************
!
!  PURPOSE:     Returns the next field from line.  If it is not an integer
!               field, an error results.
!
!  ARGUMENTS:
!     IOUT      output Returned integer field
!     IERR      output Returned non-zero if field is not an integer field
!
!***********************************************************************

      USE FREEFIELD_MODULE

      IMPLICIT NONE

      INTEGER IOUT
      INTEGER IERR

      INTEGER KTYPE

      IF (IFLD < NUMFLDS) THEN
         IFLD = IFLD + 1
         KTYPE = ITYPFLDS(IFLD)
         IF (KTYPE == 2) THEN
            IOUT = IFLDS(IFLD)
         ELSE
            IERR = IERR + 1
            IF (ILOG > 0) WRITE (ILOG,10000) CFLDS(IFLD)
         END IF

      ELSE
         KTYPE = -999
         IOUT  = 0
         IERR = IERR + 1
         IF (ILOG > 0) WRITE (ILOG,10000) 'EOF'
      END IF

      RETURN
10000 FORMAT (' *** Integer expected, not ', A)
      END


      SUBROUTINE NEXT_R (ROUT, IERR)
!***********************************************************************
!
!  PURPOSE:     Returns the next field from line.  If it is not a real
!               field, an error results.
!
!  ARGUMENTS:
!     ROUT      output Returned real field
!     IERR      output Returned non-zero if field is not a real field
!                      (integer field is converted to real)
!
!***********************************************************************

      USE FREEFIELD_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION ROUT  !apg REAL
      INTEGER IERR

      INTEGER KTYPE

      IF (IFLD < NUMFLDS) THEN
         IFLD = IFLD + 1
         KTYPE = ITYPFLDS(IFLD)
         IF ((KTYPE == 1) .OR. (KTYPE == 2)) THEN
            ROUT = RFLDS(IFLD)
         ELSE
            IERR = IERR + 1
            IF (ILOG > 0) WRITE (ILOG,10000) CFLDS(IFLD)
         END IF

      ELSE
         KTYPE = -999
         ROUT  = 0.0
         IERR = IERR + 1
         IF (ILOG > 0) WRITE (ILOG,10000) 'EOF'
      END IF

      RETURN
10000 FORMAT (' *** Real number expected, not ', A)
      END
