      MODULE SCEN_INFO_MODULE
!***********************************************************************
!
!  PURPOSE:     Contains information about the scenarios, vectors,
!               cavities, and intrusions for this run.
!
!  UPDATED:     xx-SEP-2004  --APG: Created
!
!***********************************************************************

! *** Scenarios are in the order they should be written to the output data file!

      INTEGER NUMSCENS, NUMVECS, NUMCAVS, NDIMHITS
      ! NUMSCENS = Number of scenarios
      ! NUMVECS  = Number of vectors per scenario
      ! NUMCAVS  = Number of cavities
      ! NDIMHITS = Maximum (dimensioned) number of hits per scenario

      INTEGER, ALLOCATABLE :: NUMHITS(:)
      ! NUMHITS(numscens) = Number of hits per scenario
      DOUBLE PRECISION, ALLOCATABLE :: TIMHIT(:,:)  !apg REAL
      ! TIMHIT(ndimhits,numscen) = Intrusion times (in years) for each scenario

      INTEGER, ALLOCATABLE :: ISCENID(:)
      ! ISCENID(numscens) = Scenario identifier (1 digit) for each scenario

      CHARACTER*8, ALLOCATABLE :: CAVABBR(:)
      CHARACTER*8, ALLOCATABLE :: CAVABBRl(:)  !apg
      ! CAVABBR(numcavs) = Abbreviation for each cavity, uppercase
      ! CAVABBRl(numcavs) = Abbreviation for each cavity as added to filename
      !    (may be lowercase)

      !..Current scenario and vector.  Note that cavity and intrusion
      !..loops are usually internal to specific routines

      INTEGER ICURSCEN, ICURVEC, NHITS
      ! ICURSCEN = Current scenario
      ! ICURVEC = Current vector
      ! NHITS = Number of hits for current scenario

      END MODULE SCEN_INFO_MODULE
