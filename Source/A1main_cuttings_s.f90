      PROGRAM CUTTINGS_S
!***********************************************************************
!
!
!                               CUTTINGS_S
!                               ----------
!
!                      A Borehole Drilling Program
!
!                               MARCH 1995
!
!                                Authors
!                                -------
!
!                           Jerry W. Berglund
!                       Applied Mechanics Division
!               NEW MEXICO ENGINEERING RESEARCH INSTITUTE
!                       University of New Mexico
!
!                            Robert A. Cole
!                       Applied Mechanics Division
!               NEW MEXICO ENGINEERING RESEARCH INSTITUTE
!                       University of New Mexico
!
!                           Jonathan S. Rath
!                       Applied Mechanics Division
!               NEW MEXICO ENGINEERING RESEARCH INSTITUTE
!                       University of New Mexico
!
!
!                               Abstract
!                               --------
!
!  The CUTTINGS_S code was written to calculate the quantity of material
!  (in m**3) brought to the surface from a radioactive waste disposal
!  repository as a consequence of an inadvertent human intrusion through
!  drilling.  The code determines the amount of material removed from the
!  repository by several release mechanisms, including cuttings, cavings,
!  and spallings.  The code ages the cuttings to the intrusion time.
!
!                              Limitations
!                              -----------
!
!  The CUTTINGS code is a computational procedure developed to
!  address the problem of the direct removal of wastes from a deeply
!  buried nuclear waste repository caused by future oil or gas
!  drilling activities.  The resulting computer code calculates the
!  drilling mud fluid shear stress acting on the borehole wall and
!  the subsequent erosion of repository material. The total volume
!  of material removed by drilling is the sum of the eroded material
!  and that directly cut by the drill bit. Multiple borehole intrusions
!  are permissible.  The code is based on an exact analytical solution
!  for laminar helical flow of a non-newtonian fluid in an annulus and
!  on empirical equations for turbulent flow. Input for the code
!  includes the rotational speed of the drill string, drilling mud
!  flow rate, cutting bit diameter, the shear rate dependent viscosity
!  parameters for the drilling mud, borehole roughness, the compacted
!  repository thickness and porosity, the effective failure shear
!  strength of the compacted repository material, and the number of
!  intrusions.
!
!  If the Reynolds number is greater than 2100 the calculation is
!  based on turbulent, axial, annular flow with a rotation correction.
!  If the Reynolds is less than 2100 the calculation assumes that the
!  flow is laminar and is governed by equations for the helical flow
!  of a non-newtonian fluid.  An Oldroyd type fluid is assumed.
!
!
!                         Primary Reference
!                         -----------------
!
!  Berglund, J.W. 1993,"Mechanisms Governing the Direct Removal of
!  Wastes from the WIPP Repository Caused by Exploratory Drilling",
!  SAND92-7295, Albuquerque, NM: Sandia National Laboratories.
!
!                        Secondary References
!                        --------------------
!
!  Rath, J.S. 1994,"User's Reference Manual for CUTTINGS: A Code for
!  Computing the Quantity of Wastes Brought to the Ground Surface
!  When a Waste Disposal Zone of a Radioactive Waste Repository is
!  Inadvertently Penetrated by an Exploratory Borehole", SAND94-XXXX,
!  Albuquerque, NM: Sandia National Laboratories.
!
!                           Laminar Flow
!                           ------------
!
!  Savins,J.G. and Wallick, G.C.,"Viscosity Profiles, Discharge
!  Rates,  Pressures, and Torques for a Rheologically Complex Fluid
!  in a Helical Flow", A.I.CH.E. Journal, Vol. 12, No. 2, March 1966.
!
!                           Turbulent Flow
!                           --------------
!
!  "Theory and Application of Drilling Fluid Hydraulics", Whittaker, A.,
!   Editor, International Human Resources Development Corporation,
!   EXLOG, Boston,1985.
!
!  "Drilling Mud and Cement Slurry Rheology Manual", Broc, R., Editor,
!   Gulf Publishing Company, Houston, 1982.
!
!
!                             Update History
!                             --------------
!
!  VERSION   DATE           MODIFIER        CHANGES
!
!apg  6.03   09/xx/12       *** Migrate from Alpha OpenVMS to Solaris ***
!apg                        Modified by Amy Gilkey (see !apg comments)
!apg                        All real variables are now double precision.
!apg                        Minor code cleanup, such as allowing Unix
!apg                         file names or using "trim" to print strings.
!apg                        Minor cosmetic changes may be visible in output.
!  6.01      xx-APR-2005    A.P.Gilkey      -Use vector number as spall vector
!                                            (unless rndspall input)
!  6.00      xx-SEP-2004    A.P.Gilkey      -Rewrite;
!                                            Remove radionuclide calcs;
!                                            Remove calibration mode;
!                                            Remove pre-cuttings;
!                                            Remove Model1 and Model2;
!                                            TXT0 file is no longer needed;
!                                            Update to FORTRAN90 (dynamic
!                                            memory, loops, etc);
!                                            Run all scenarios, vectors,
!                                            cavities, times in single run;
!                                            Output data file;
!                                            Add read from master control file
!  5.05      06-OCT-2003    C.W.Hansen      -Added Model4 for SPALLINGS
!  5.0x      xx-MAY-1997    A.P.Gilkey      -Added Model3;
!                                            Add PERMBRIN from BRAGFLO CAMDAT
!  5.00      02-FEB-1996    R.A.Cole        -Added model2 for SOLID_OUT
!                                            CALIBRATION mode
!  C-4.00VV  MAR-JUN-1995   R.A.Cole        -CUTTINGS became CUTTING_SPA
!                                            new models were implemented
!                                            the code to reflect the
!                                            mechanisms of blowout, stuc
!                                            pipe, and gas erision. New
!                                            subroutines added were:
!                                              GASBLOW
!                                             GASERS
!                                             AREASP
!                                            subroutine modified extensi
!                                             SCATXT
!                                             REATXT
!                                             PREPRO
!
!  C-3.24ZO   25-AUG-1994   J.W.Berglund    -Changed flowrate constant
!                                            in DRILL.FOR
!  C-3.23ZO   17-MAY-1994   J.S.Rath        -Added write statements
!                                            to debug/diagnostics file:
!                                            in MODULE SCATXT.FOR
!  C-3.22ZO   10-MAY-1994   J.S.Rath        -Added optional output
!                                            files creation control
!                                            options for:
!                                            RADIO-ISOTOPE history &
!                                            NORMALIZED RADIO-ISOTOPE
!                                            HISTORY files.
!                                            (modified TOOLS.INC &
!                                             PREPRO.FOR).
!  C-3.21ZO   29-MAR-1994    J. S. Rath     -Added write statements
!                                            in modules: DRILL and
!                                            INTERFACE. (writing
!                                            to debug/diagnostics file)
!  C-3.20ZO   21-MAR-1994    J. S. Rath     -Fixed error in computing
!                                            SUMCI(*) array; erroneous
!                                            addition of HIT#0.
!                                           -Added capability in
!                                            input text file w.r.t.
!                                            saving RAD. values to
!                                            output CAMDAT & debug/
!                                            diagnostics file.
!  C-3.11ZO   23-FEB-1994    J. S. Rath     -Fixed "BUG" in GENCUT
!                                            w.r.t. XMOLES(*) array
!                                            interfacing w/new decay
!                                            chains function QUANKG.
!  C-3.10ZO   21-FEB-1994   J. S. Rath      -Adapted to read a
!                                            GENERIC RADIOISOTOPE
!                                            DATA FILE.
!                                           -Changed decay logic
!                                            to HJIUZZO's algorithm
!                                            based on decay chains.
!                                           -Changed to read
!                                            user/expert defined
!                                            radioisotope decay
!                                            chains from input
!                                            text file.
!                                           -Corrected constant
!                                            used for computing the
!                                            Activity Conversion.
!  C-3.03ZO   28-JAN-1994   J. S. Rath      -Improved EPA 40CFR191
!                                            Subpart B file for
!                                            "Normalization".
!  C-3.02ZO   18-JAN-1994   J. S. Rath      -Corrected "NORMALIZATION"
!                                            w.r.t. EPA 40 CFR 191 $13
!                                            standard.
!  C-3.01ZO   07-DEC-1993   J. S. Rath      -Improved radioisotope
!                                            decay logic (checks
!                                            "stable" isotopes)
!                                           -Adapted for 232 isotopes.
!  C-3.00ZO   11-NOV-1993   J. S. Rath      -Reads radioisotope data
!                                            from the SECONDARY DATA
!                                            BASE.
!                                           -Adapted to latest
!                                            CAMDAT_LIB, CAMCON_LIB,
!                                            and CAMSUPES_LIB routines.
!                                           -Modified to handle both
!                                            WIPP and INEL/WINCO
!                                            RAD-waste.
!                                           -Simplified and streamlined
!                                            program flow logic, etc...
!                                           -Added more filenames
!                                            for I/O (command line).
!                                           -Installed on "ALPHA"
!                                            machine (note 3.00ZO, where
!                                            "Z" denotes ALPHA-AXP &
!                                            "O" deontes OpenVMS).
!  C-2.31VV   28-OCT-1993   J. W. Berglund  -Modified equation for
!                                            initial selection of
!                                            parameter CC used in
!                                            INTERFACE subroutine
!  C-2.30VV   12-MAY-1993   J. W. Berglund  -Added subroutine INTERFACE
!                                            that calculates an axial
!                                            flow correction factor to
!                                            account for drillstring
!                                            rotation in
!                                            the turbulent flow regime
!  C-2.23VV   22-APR-1993   J. W. Berglund  -Corrected transition from
!                                            turbulent to laminar flow
!                                            when calculated ROUTER
!                                            exceeds critical radius.
!  C-2.22VV   25-FEB-1993   J. S. Rath      -Added call to DBSETUP.
!                                            Does not affect version
!                                            C-2.21VV because it does
!                                            not change the CUTTINGS
!                                            flow logic or I/O.
!                                            Call to DBSETUP is now
!                                            required and necessary
!                                            when using CAMDAT_LIB
!                                            routines.
!  C-2.21VV   14 DEC-1992   J. W. Berglund  -Corrected friction
!                                            function for turbulant
!                                            flow.
!  C-2.20VV   11-AUG-1992   J. S. Rath      -Added capability to
!                                            extract all CUTTINGS
!                                            input variables from
!                                            the input CAMDAT file.
!  C-2.10VV   30-JUL-1992   J. S. Rath      -Adapted entire program
!                                            to interface with
!                                            CAMDAT_LIB routines.
!  C-2.09VV   23-JUL-1992   J. W. Berglund  -Added variables VISCO
!                                            and YLDSTRSS based on
!                                            Bingham drilling fluid
!                                            data to calculate
!                                            Oldroyd properties.
!  C-2.08VV   13-JUL-1992   J. W. Berglund  -Removed variables THICK,
!                                            EVOLUME,UPHOL,WVOLUME
!                                            from TRANS,TRANSL1 --
!                                            changed READIN call.
!  C-2.07VV   07-JUL-1992   J. W. Berglund  -Corrected Turbulent flow
!                                            equations (0.8165 factor)
!  C-2.06VV   22-JAN-1992   J. W. Berglund  -Modified Debug File Output
!  C-2.05VV   25-SEP-1991   J. W. Berglund  -Replaced POROSITY variable
!                                            with DIAMMOD
!  C-2.04VV   17-JUL-1991   J. W. Berglund  -Added write statements
!                                           -Changed CIPMSQ in BLOCK.FOR
!  C-2.03VV   29-JAN-1991   J. W. Berglund  -Added write statements
!                                           -Moved seed C,RJ2,LAMBDA
!                                            into 1000 loop
!  C-2.02VV   03-JAN-1991   J. S. Rath      -Added write statements
!                                            in TRANSL.FOR; Fixed
!                                            character declaration
!                                            in CUTVAR.INC; Changed
!                                            lowercase keywords to
!                                            uppercase in
!                                            READIN.FOR; Fixed
!                                            error in TRANSL wrt
!                                            NHITS. Changes in
!                                            GENCUT,TRANSL,GCUNAM.
!  C-2.01VV   14-DEC-1990   J. S. Rath      -Eliminated writing
!                                            CUTTINGS input text
!                                            file vars. to CAMDAT.
!                                            RE-ORDERED the VALVAR
!                                            array.  Added Input
!                                            text file keywords
!                                            (*MAT/OUT_MAT).
!  C-2.00VV   01-DEC-1990   J. W. Berglund  -Added Harold Iuzzolino's
!                                            GENCUTDATA code to calc.
!                                            radionuclides removed by
!                                            drilling.
!  C-1.22VV   08-NOV-1990   J. W. Berglund  -Added optional user
!                                            input text file &
!                                            QA help file.
!  X-1.21VV   02-OCT-1990   J. W. Berglund  -Added repository
!                                            thickness and porosity
!  X-1.20VV   12-SEP-1990   J. W. Berglund  -Added turbulent flow
!  X-1.11VV   03-JUL-1990   J. W. Berglund  -Input variables changed
!  X-1.10VV   02-JUL-1990   J. S. Rath      -Adapted to CAMCON
!  X-1.00VV   03-MAY-1990   J. W. Berglund  -First Ed. from NMERI
!                                            Harris HCX-9; installed
!                                            on SNL 8810 VAX
!
!
!                             Disclaimer
!                             ----------
!
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
!                    ISSUED BY SANDIA LABORATORIES,                   *
!                      A PRIME CONTRACTOR TO THE                      *
!                  UNITED STATES DEPARTMENT OF ENERGY                 *
! * * * * * * * * * * * * *   N O T I C E   * * * * * * * * * * * * * *
! This program was prepared as an account of work  sponsored  by  the *
! United States Government.  Neither the United States nor the United *
! States Department of Energy nor any of their employees, nor any  of *
! their  contractors,  subcontractors,  or their employees, makes any *
! warranty, express or implied, or assumes  any  legal  liability  or *
! responsibility  for the accuracy, completeness or usefulness of any *
! information, apparatus, product or process disclosed, or represents *
! that its use would not infringe privately owned rights.             *
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
!
!
!                            Files used
!                            ----------
!
!  Input master control file
!  Input control files (one for each scenario)
!  Input CAMDAT files (one for each scenario-vector pair)
!  Input BRAGFLO CAMDAT files (one for each scenario-vector pair)
!  Input spall model 4 data file
!  Output CAMDAT files (one for each scenario-vector-cavity-intrusion)
!  Output data file
!  Output debug file
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE BRAG_SIZING_MODULE
      USE CDB_PROPS_MODULE
      USE DEBUG_MODULE
      USE MODEL_PARAMS_MODULE
      USE MODEL4_DATA_MODULE
      USE OUTPUT_DATA_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER IERR
      INTEGER IDUM
      DOUBLE PRECISION CPUSEC  !apg REAL

! File names and unit numbers
      CHARACTER*132 FIMASTER, FITXT1, FICDB, FIBRAG, FISPL4, FODAT, FOCDB
      CHARACTER*132 XITXT1, XICDB, XIBRAG, XISPL4
      INTEGER IMASTER, ITXT1, ICDB, IBRAG, ISPL4, OCDB, ODAT
      PARAMETER (IMASTER = 10)
      PARAMETER (ITXT1   = 11)
      PARAMETER (ICDB    = 12)
      PARAMETER (IBRAG   = 13)
      PARAMETER (ISPL4   = 14)
      PARAMETER (OCDB    = 20)
      PARAMETER (ODAT    = 21)
      ! PARAMETER (ODBG  = 29)  ! Set in DEBUG_MODULE
      ! IMASTER = Unit number of input master control file
      ! ITXT1 = Unit number of input control file
      ! ICDB = Unit number of input CAMDAT file
      ! IBRAG = Unit number of input BRAGFLO CAMDAT file
      ! ISPL4 = Unit number of input spall model 4 data file
      ! OCDB = Unit number of output CAMDAT file
      ! ODAT = Unit number of output data file

! Dynamic memory arrays needed for DBSETUP (CAMDAT_LIB)
      REAL RQ(1)  !apg must be REAL
      CHARACTER*(1) CQ(1)

!**** EXECUTABLE STATEMENTS ****

!**** Initialize ****

      !..Read names of files; open output debug file
      CALL PREPRO (FIMASTER, ODBG)

      !..Initialize dynamic memory base arrays
      CALL MDINIT (RQ)
      CALL MCINIT (CQ)

      !..Set up the dynamic memory base arrays for CAMDAT access
      CALL DBSETUP (RQ, CQ, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Calling DBSETUP')

      !..Read scenarios, intrusion times, etc. from master control file

      !..Open master control file
      CALL FILOPEN (IMASTER, 'INP', 'FORM', FIMASTER, IERR)
      IF (IERR /= 0) CALL QAABORT &
         ('Opening input control file')

      !..Read scenarios, intrusion times, etc. and file name templates
      CALL READ_MASTER (FIMASTER, IMASTER, &
         FITXT1, FICDB, FIBRAG, FISPL4, FODAT, FOCDB)

      !..Set flag if BRAGFLO CAMDAT specified
      BRAGCDB = (FIBRAG /= ' ')

      !..Close master control file
      CLOSE (IMASTER, IOSTAT=IERR)

      !..Reserve dynamic arrays for values (results and input values)
      !..that must be in output data file
      ALLOCATE (DIAMMOD(1:NDIMHITS,1:NUMCAVS,1:NUMVECS,1:NUMSCENS), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'DIAMMOD', IERR)
      ALLOCATE (REPPRES(1:NDIMHITS,1:NUMCAVS,1:NUMVECS,1:NUMSCENS), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'REPPRES', IERR)
      ALLOCATE (AREAC(1:NDIMHITS,1:NUMCAVS,1:NUMVECS,1:NUMSCENS), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'AREAC', IERR)
      ALLOCATE (VOLS(1:NDIMHITS,1:NUMCAVS,1:NUMVECS,1:NUMSCENS), STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'VOLS', IERR)

!**** Loop over each scenario ****

      DO ICURSCEN = 1, NUMSCENS
         NHITS = NUMHITS(ICURSCEN)
         IF (NHITS <= 0) CYCLE

!**** Pre-process the input control file and set up memory ****

         WRITE (*,10020) ISCENID(ICURSCEN), NUMVECS, NUMCAVS, NHITS

         !..Setup defaults
         CALL INIT_DEFAULTS

         !..Open input control file
         CALL EXPAND_TEMPLATE (FITXT1, .TRUE., &
            ISCENID(ICURSCEN), -1, '#', -1, XITXT1)
         CALL FILOPEN (ITXT1, 'INP', 'FORM', XITXT1, IERR)
         IF (IERR /= 0) CALL QAABORT &
            ('Opening input control file')

         !..Scan input control file
         CALL SCAN_CONTROL (ITXT1)

         !..Reserve dynamic arrays for BRAGFLO CAMDAT averages
         ALLOCATE (POROSITY(1:NDIMZONES,0:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'POROSITY', IERR)
         ALLOCATE (PRESGAS(1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'PRESGAS', IERR)
         ALLOCATE (PRESBRIN(1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'PRESBRIN', IERR)
         ALLOCATE (SATGAS(1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'SATGAS', IERR)
         ALLOCATE (PERMBRIN(1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'PERMBRIN', IERR)
         ALLOCATE (VALBFVAR(1:NBFVAR,1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'VALBFVAR', IERR)

         !..Reserve dynamic arrays for calculated output from averages
         ALLOCATE (POROF(1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'POROF', IERR)
         ALLOCATE (HFINAL(1:NDIMZONES,1:NHITS,1:NUMCAVS), STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.TRUE., 'HFINAL', IERR)

!**** Loop over each vector ****

         DO ICURVEC = 1, NUMVECS
            WRITE (*,10030) ISCENID(ICURSCEN), ICURVEC
            WRITE (ODBG,10000)
            WRITE (ODBG,10010) ISCENID(ICURSCEN), ICURVEC

!**** Process input control file for first vector only; otherwise just ****
!**** read parameters from input CAMDAT file                           ****

            !..Read input CAMDAT file for items needed for processing
            !..input control file

            !..Open input CAMDAT file
            CALL EXPAND_TEMPLATE (FICDB, .TRUE., &
               ISCENID(ICURSCEN), ICURVEC, '#', -1, XICDB)
            CALL DBIOPEN (ICDB, XICDB, IDUM, IDUM, IERR)
            IF (IERR /= 0) CALL QAABORT &
               ('Opening input CAMDAT file')

            !..Read properties from input CAMDAT file
            CALL READ_CDB_PROPS (ICDB)

            !..Open BRAGFLO CAMDAT file
            IF (BRAGCDB) THEN
               CALL EXPAND_TEMPLATE (FIBRAG, .TRUE., &
                  ISCENID(ICURSCEN), ICURVEC, '#', -1, XIBRAG)
               CALL DBIOPEN (IBRAG, XIBRAG, IDUM, IDUM, IERR)
               IF (IERR /= 0) CALL QAABORT &
                  ('Opening input BRAGFLO CAMDAT file')
            END IF

            IF (ICURVEC == 1) THEN
               !..Read BRAGFLO CAMDAT file for items needed for processing
               !..input control file
               IF (BRAGCDB) THEN
                  CALL PREPRO_BRAG (IBRAG)
               END IF

               !..Read input control file
               CALL READ_CONTROL (XITXT1, ITXT1)

               !..Close input control file
               CLOSE (ITXT1, IOSTAT=IERR)

               IF (MODTYPE == 'MODEL4') THEN
                  !..Open spall model 4 data file
                  CALL EXPAND_TEMPLATE (FISPL4, .TRUE., &
                     ISCENID(ICURSCEN), -1, '#', -1, XISPL4)
                  CALL FILOPEN (ISPL4, 'INP', 'FORM', XISPL4, IERR)
                  IF (IERR /= 0) CALL QAABORT &
                     ('Opening Spall Model 4 data file')

                  !..Read spall model 4 data file
                  CALL READ_MODEL4_DATA (XISPL4, ISPL4, NUMVECS, IERR)

                  IF (IERR /= 0) CALL QAABORT &
                     ('Reading Spall Model 4 data file')

                  !..Close spall model 4 data file
                  CLOSE (ISPL4, IOSTAT=IERR)
               END IF

               IF (BRAGCDB) THEN
                  !..Check items requested from BRAGFLO CAMDAT file
                  CALL CHECK_BRAG (NUMCAVS, IERR)
                  IF (IERR /= 0) CALL QAABORT &
                     ('Invalid BRAGFLO CAMDAT file items requested')

                  !..Release BRAGFLO CAMDAT file items only needed for
                  !..processing input control file
                  DEALLOCATE (NAMELB_B, STAT=IERR)
                  IF (IERR /= 0) &
                     CALL HANDLE_MEMORY_ERROR (.FALSE., 'NAMELB_B', IERR)
                  DEALLOCATE (NUMLNK_B, STAT=IERR)
                  IF (IERR /= 0) &
                     CALL HANDLE_MEMORY_ERROR (.FALSE., 'NUMLNK_B', IERR)
                  DEALLOCATE (NMATPR_B, STAT=IERR)
                  IF (IERR /= 0) &
                     CALL HANDLE_MEMORY_ERROR (.FALSE., 'NMATPR_B', IERR)
                  DEALLOCATE (NAMATR_B, STAT=IERR)
                  IF (IERR /= 0) &
                     CALL HANDLE_MEMORY_ERROR (.FALSE., 'NAMATR_B', IERR)
                  DEALLOCATE (NAMEVR_B, STAT=IERR)
                  IF (IERR /= 0) &
                     CALL HANDLE_MEMORY_ERROR (.FALSE., 'NAMEVR_B', IERR)
               END IF
            END IF

            !Get parameters from input CAMDAT file
            CALL VECTOR_CONTROL (XICDB)

!**** Note that loop over cavity and intrusion is internal to routines ****

!**** Read BRAGFLO CAMDAT for averaged variables ****

            IF (BRAGCDB) THEN
               !..Read variables from BRAGFLO CAMDAT file
               CALL READ_BRAG (XIBRAG, IBRAG, IERR)
               IF (IERR /= 0) CALL QAABORT &
                  ('Reading BRAGFLO CAMDAT file')

               !..Close BRAGFLO CAMDAT file
               CALL DBICLOSE (IBRAG, IERR)
            END IF

            !..Calculate final porosity and height
            CALL FINALPORO

!**** Calculate the cuttings, cavings, spallings ****

            !..Calculate cuttings, cavings, spallings
            CALL CUTSPALL

!**** Write the BRAGFLO and cuttings output to the CAMDAT files ****

            !..Write the cuttings output to one or more output CAMDAT files
            CALL WRITE_CAMDATS (ICDB, FOCDB, OCDB)

            !..Close input CAMDAT file
            CALL DBICLOSE (ICDB, IERR)

            !Delete dynamic array for input CAMDAT file
            DEALLOCATE (NAMELB, STAT=IERR)
            IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'NAMELB', IERR)
            DEALLOCATE (NMATPR, STAT=IERR)
            IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'NMATPR', IERR)
            DEALLOCATE (IASPRP, STAT=IERR)
            IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'IASPRP', IERR)
            DEALLOCATE (XMATPR, STAT=IERR)
            IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'XMATPR', IERR)
         END DO

!**** Perform ending procedures for scenario loop ****

         !..Delete dynamic arrays for BRAGFLO CAMDAT averages
         DEALLOCATE (POROSITY, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'POROSITY', IERR)
         DEALLOCATE (PRESGAS, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'PRESGAS', IERR)
         DEALLOCATE (PRESBRIN, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'PRESBRIN', IERR)
         DEALLOCATE (SATGAS, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'SATGAS', IERR)
         DEALLOCATE (PERMBRIN, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'PERMBRIN', IERR)
         DEALLOCATE (VALBFVAR, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'VALBFVAR', IERR)

         !..Delete dynamic arrays for calculated output from averages
         DEALLOCATE (POROF, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'POROF', IERR)
         DEALLOCATE (HFINAL, STAT=IERR)
         IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'HFINAL', IERR)

         !..Delete misc dynamic arrays
         IF (MODTYPE == 'MODEL4') THEN
            DEALLOCATE (PRES, STAT=IERR)
            IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'PRES', IERR)
            DEALLOCATE (SPLVOL, STAT=IERR)
            IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'SPLVOL', IERR)
         END IF
      END DO

!**** Write output data file ****

      WRITE (*,*)
      WRITE (*,10040) 'Writing results to output data file'

      !..Write the cuttings output to output data file
      CALL WRITE_OUTPUT (FODAT, ODAT)

      !..Delete dynamic arrays for output data file values
      DEALLOCATE (DIAMMOD, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'DIAMMOD', IERR)
      DEALLOCATE (REPPRES, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'REPPRES', IERR)
      DEALLOCATE (AREAC, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'AREAC', IERR)
      DEALLOCATE (VOLS, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'VOLS', IERR)

      !..Delete misc dynamic arrays
      DEALLOCATE (NUMHITS, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'NUMHITS', IERR)
      DEALLOCATE (TIMHIT, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'TIMHIT', IERR)
      DEALLOCATE (CAVABBR, STAT=IERR)
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'CAVABBR', IERR)
      DEALLOCATE (CAVABBRl, STAT=IERR)  !apg
      IF (IERR /= 0) CALL HANDLE_MEMORY_ERROR (.FALSE., 'CAVABBRl', IERR)

      !..Closing procedures
      WRITE (ODBG,10000)
      WRITE (*,*)
      CALL QACPUS (ODBG, CPUSEC)
      CALL QAPAGE (ODBG, 'END')
      CLOSE (ODBG, IOSTAT=IERR)

      STOP 'CUTTINGS_S Normal Completion'

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/)
10010 FORMAT (20X, '*** SCENARIO', I2, ' *** VECTOR', I4, ' ***')
10020 FORMAT (/, 1X, 'SCENARIO', I2, ':', &
         I5, ' Vectors,', I5, ' Cavities,', I5, ' Intrusions')
10030 FORMAT (4X, 'Scenario', I2, 4X, 'Vector', I4)
10040 FORMAT (1X, A)

      END PROGRAM CUTTINGS_S
