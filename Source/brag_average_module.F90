      MODULE BRAG_AVERAGE_MODULE
!***********************************************************************
!
!  PURPOSE:     Contains information about the BRAGFLO CAMDAT averages.
!               This includes the elements to be averaged for each zone,
!               the names of the variables to be averaged, and the averaged
!               values.
!
!  UPDATED:     xx-SEP-2004  --APG: Created from RDBRAG.INC
!
!***********************************************************************

      LOGICAL BRAGCDB
      ! BRAGCDB = TRUE iff BRAGFLO CAMDAT file is read

! Description of the zones to average BRAGFLO CAMDAT element
! variables over, including repository pressure

      INTEGER MAXZONES
      PARAMETER (MAXZONES=10)
      ! MAXZONES = Maximum number of zones with averaged values
      INTEGER MAXCAVITIES
      PARAMETER (MAXCAVITIES=10)
      ! MAXCAVITIES = Maximum number of cavities
      INTEGER MAXALLZONES
      PARAMETER (MAXALLZONES=50)
      ! MAXALLZONES = Maximum number of zones for all cavities
      INTEGER MAXZNSETS
      PARAMETER (MAXZNSETS=50)
      ! MAXZNSETS = Maximum number of element sets that define each
      !    repository zone

      INTEGER NDIMZONES
      ! NDIMZONES = Dimension of zones for BRAGFLO averaged variables

      INTEGER NZONES(MAXCAVITIES)
      ! NZONES = Number of zones for BRAGFLO averaged variables

      INTEGER IXZONES(MAXZONES,MAXCAVITIES)
      ! IXZONES - the index (into NELSETS, etc) for zones for each cavity
      INTEGER IXZONES_P(MAXCAVITIES)
      ! IXZONES_P - the index (into NELSETS_P, etc) for zones for repository
      !    pressure for each cavity

      INTEGER NELSETS(MAXALLZONES), &
         IELBEG(MAXZNSETS,MAXALLZONES), IELEND(MAXZNSETS,MAXALLZONES)
      LOGICAL WTAVG(MAXALLZONES)
      ! NELSETS = The number of element sets that define repository zone
      ! IELBEG = Starting element number for repository zone
      ! IELEND = Ending element number for repository zone
      ! WTAVG = TRUE if volume-weighted average of variables to be done,
      !    FALSE if simple average is to be done

      INTEGER NELSETS_P(MAXCAVITIES), &
         IELBEG_P(MAXZNSETS,MAXCAVITIES), IELEND_P(MAXZNSETS,MAXCAVITIES)
      LOGICAL WTAVG_P(MAXCAVITIES)
      ! The "_P" variables are for REPPRES only

! CAMDAT names of the needed items on the BRAGFLO CAMDAT file,
! including the names of the element variables to be averaged

      CHARACTER*8 NAMGRIDVOL
      ! NAMGRIDVOL = BRAGFLO CAMDAT attribute name of grid volume

      INTEGER NBFREQ
      PARAMETER (NBFREQ=5)
      ! NBFREQ = Number of required BRAGFLO CAMDAT element variables to read:
      !    POROSITY, PRESGAS, PRESBRIN, SATGAS, PERMBRIN
      CHARACTER*8 NAMPOROSITY, NAMPRESGAS, NAMPRESBRIN, &
         NAMSATGAS, NAMSATBRIN, NAMPERMBRIN
      ! NAMPOROSITY = BRAGFLO CAMDAT variable name of porosity
      ! NAMPRESGAS  =     "                   "    of gas pressure
      ! NAMPRESBRIN =     "                   "    of brine pressure
      ! NAMSATGAS   =     "                   "    of sat pressure (gas)
      ! NAMSATBRIN  =     "                   "    of sat pressure (brine)
      ! NAMPERMBRIN =     "                   "    of permeability (brine)

      INTEGER MAXBFVAR
      PARAMETER (MAXBFVAR=10)
      ! MAXBFVAR = Maximum number of user-selected BRAGFLO CAMDAT element
      !    variables to read
      INTEGER NBFVAR
      ! NBFVAR = Number of user-selected BRAGFLO CAMDAT element
      !    variables to read
      CHARACTER*8 NAMBFVAR(MAXBFVAR)
      ! NAMBFVAR = BRAGFLO CAMDAT name of user-selected variables to read

      CHARACTER*8 NAMREPPRES
      ! NAMREPPRES = BRAGFLO CAMDAT variable name of repository pressure

! Volume-weighted averages of the BRAGFLO CAMDAT values

      DOUBLE PRECISION, ALLOCATABLE :: POROSITY(:,:,:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: PRESGAS(:,:,:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: PRESBRIN(:,:,:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: SATGAS(:,:,:)  !apg REAL
      !  Note that SATBRIN is calculated as 1.0-SATGAS
      DOUBLE PRECISION, ALLOCATABLE :: PERMBRIN(:,:,:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: VALBFVAR(:,:,:,:)  !apg REAL
      ! All indexed by (zone, intrusion, cavity)
      ! POROSITY(ndimzones,nhits,numcavs) = Porosity
      ! PRESGAS(ndimzones,nhits,numcavs)  = Pressure in gas
      ! PRESBRIN(ndimzones,nhits,numcavs) = Pressure in brine
      ! SATGAS(ndimzones,nhits,numcavs)   = Saturation of gas
      ! PERMBRIN(ndimzones,nhits,numcavs) = Permeability of brine
      ! VARBFVAR(ndimzones,nhits,numcavs) = User-specified variable(s)

! Calculated values; using the porosity and repository height

      DOUBLE PRECISION, ALLOCATABLE :: POROF(:,:,:)  !apg REAL
      DOUBLE PRECISION, ALLOCATABLE :: HFINAL(:,:,:)  !apg REAL
      ! POROF(ndimzones,nhits,numcavs)    = Final porosity at intrusion time
      ! HFINAL(ndimzones,nhits,numcavs)   = Final height at intrusion time

      END MODULE BRAG_AVERAGE_MODULE
