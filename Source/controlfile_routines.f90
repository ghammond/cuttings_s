      SUBROUTINE SCAN_CONTROL (ITXT1)
!***********************************************************************
!
!  PURPOSE:     Scans the CUTTINGS input control file for program array
!               sizing parameters.
!
!  AUTHOR:      Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of SCATXT1;
!                              Moved all non-sizing command processing to
!                              READ_CONTROL
!               06-OCT-2003  --CWH: Added input for Model4
!               02-FEB-1996  --RAC: Added inputs for model2
!               17-MAY-1994  --JSR: Added more write statements
!                                   to diagnostics/debug file
!               21-MAR-1994  --JSR: Adapted to new input text file
!               17-FEB-1994  --JSR: Adapted to new input text file
!               05-NOV-1993  --JSR: First Ed.
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     ITXT1     input  Input control file
!
!  Sets NDIMZONES in BRAG_AVERAGE_MODULE
!  Sets NBFVAR in BRAG_AVERAGE_MODULE
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE DEBUG_MODULE
      USE MODEL_PARAMS_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER ITXT1

      INTEGER IERR, IZONE, IZONECT
      INTEGER INUM, IOSTAT, ITYPE
      DOUBLE PRECISION RNUM  !apg REAL
      CHARACTER*18 CMD, WORD

!**** EXECUTABLE STATEMENTS ****

      IERR   = 0
      NDIMZONES = 1
      NBFVAR = 0

  100 CONTINUE
      CALL NEXTLINE (ITXT1, 0, IOSTAT)
      IF (IOSTAT /= 0) GOTO 110

      CALL NEXT_C (CMD, IERR)

      IF (CMD == 'EOF') THEN
         !..Stop at EOF
         GOTO 110

      ELSEIF (CMD == 'NOMODEL') THEN
         MODTYPE = ' '

      ELSEIF (CMD == 'MODEL3') THEN
         MODTYPE = CMD

      ELSEIF (CMD == 'MODEL4') THEN
         MODTYPE = CMD

      ELSEIF (CMD(1:5) == 'INTR_') THEN
         READ (CMD(6:), *) IZONE
         IZONE = IZONE + 1
         NDIMZONES = MAX (NDIMZONES, IZONE)

      ELSEIF (CMD(1:9) == 'NAME_BRAG') THEN
         DO WHILE (.TRUE.)
            CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
            IF (ITYPE /= 0) EXIT
            NBFVAR = NBFVAR + 1
         END DO
      END IF

      !..Scan next record
      GOTO 100

  110 CONTINUE

      IF (.NOT. BRAGCDB) THEN
         NDIMZONES = 1
      END IF

      RETURN

      END SUBROUTINE SCAN_CONTROL


      SUBROUTINE READ_CONTROL (FITXT1, ITXT1)
!***********************************************************************
!
!  PURPOSE:     Reads the CUTTINGS input control file.  Some commands are
!               processed directly, but commands that set parameters that
!               may be read from the input CAMDAT file (or the BRAGFLO
!               CAMDAT file) save the CAMDAT property location only.
!
!  AUTHOR:      Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of REATXT1;
!                              Added many commands from SCAN_CONTROL;
!                              Added new commands and removed unneeded ones;
!                              Save location of CAMDAT parameters
!               02-FEB-1996  --RAC: Added properities for model2
!               17-MAY-1994  --JSR: Added write statements to
!                                   diagnostics/debugging file
!               29-MAR-1994  --JSR: Adapted to new input text format.
!               23-FEB-1994  --JSR: Adapted to new input text format.
!                                   Added arrays.
!               05-NOV-1993  --JSR: Completely new version.
!               11-AUG-1992  --JSR: Added new keywords to permit
!                                   extracting either CONTACT or
!                                   REMOTE handled radioisotope
!                                   data.
!               29-JUL-1992  --JSR: Deleted unsed variables
!               22-JUL-1992  --JWB: Removed ETA0 and SIGMA2 in call.
!                                   Added VISCO and YLDSTRSS
!               13-JUL-1992  --JWB: Removed THICK and UPHOLEV in call
!               04-JAN-1991  --JWB: Added arguments in call statement
!               17-DEC-1990  --JSR: Changed lowercase keywords to
!                                   uppercase
!               13-DEC-1990  --JSR: Added OUTNAM to arg. list &
!                                   reordered VALVAR array
!               20-SEP-1990  --JWB: New variables added
!               03-JUL-1990  --JWB:
!               02-JUL-1990  --JWB: First Ed. for CUTTINGS
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     FITXT1    input  Filename of input control file
!     ITXT1     input  Input control file
!
!  Sets parameters locations in PARAM_LOCS_MODULE
!  Sets parameters in CONSTANTS_MODULE
!  Sets parameters in DRILL_PARAMS_MODULE
!  Sets parameters in MODEL_PARAMS_MODULE
!  Sets OUTMAT in CDB_PROPS_MODULE
!  Sets IXZONE, etc in BRAG_AVERAGE_MODULE
!  Sets DIAMMOD in OUTPUT_DATA_MODULE
!  Sets REPPRES in OUTPUT_DATA_MODULE (if no BRAGCDB)
!  Sets POROSITY and PRESGAS in BRAG_AVERAGE_MODULE (if no BRAGCDB)
!
!***********************************************************************

      USE BRAG_AVERAGE_MODULE
      USE CDB_PROPS_MODULE
      USE CONSTANTS_MODULE
      USE DEBUG_MODULE
      USE DRILL_PARAMS_MODULE
      USE MODEL_PARAMS_MODULE
      USE OUTPUT_DATA_MODULE
      USE PARAM_LOCS_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      CHARACTER*(*) FITXT1
      INTEGER ITXT1

      LOGICAL ONCDB
      INTEGER I, IERR, NLOG
      INTEGER ICAV, IX, IZONE, IXZNCT, IXZNCT_P
      INTEGER INUM, IOSTAT, ITYPE
      DOUBLE PRECISION RNUM  !apg REAL
      DOUBLE PRECISION XVAL  !apg REAL
      CHARACTER*18 CMD, WORD

! Function definitions
      INTEGER IXCDBNAM

!**** EXECUTABLE STATEMENTS ****

      IERR = 0

      !..Initialize

      OUTMAT = ' '

      IXZNCT = 0
      IXZNCT_P = 0
      DO ICAV = 1, NUMCAVS
         NZONES(ICAV) = 0
         IXZONES_P(ICAV) = 0
         DO I = 1, MAXZONES
            IXZONES(I,ICAV) = 0
         END DO
      END DO
      IF (.NOT. BRAGCDB) THEN
         DO ICAV = 1, NUMCAVS
            NZONES(ICAV) = 1
         END DO
      END IF

      DO ICAV = 1, NUMCAVS
         DO I = 1, NHITS
            DIAMMOD(I,ICAV,ICURVEC,ICURSCEN) = 0.0
         END DO
      END DO

      IF (.NOT. BRAGCDB) THEN
         DO ICAV = 1, NUMCAVS
            POROSITY(1,0,ICAV) = 0.0
            DO I = 1, NHITS
               POROSITY(1,I,ICAV) = 0.0
               PRESGAS(1,I,ICAV) = 0.0
               REPPRES(I,ICAV,ICURVEC,ICURSCEN) = 0.0
            END DO
         END DO
      END IF

      NBFVAR = 0

      WRITE (ODBG,10000) TRIM (FITXT1)
      REWIND (ITXT1)

      NLOG = ODBG

      !..Read and process each line of the input control file

  100 CONTINUE
      CALL NEXTLINE (ITXT1, NLOG, IOSTAT)
      IF (IOSTAT /= 0) GOTO 110

      CALL NEXT_C (CMD, IERR)

      IF (CMD == 'EOF') THEN
         !..Stop at EOF
         GOTO 110

!**** Model type (already read in previous scan) ****

      ELSEIF (CMD == 'NOMODEL') THEN
         !..Set in SCAN_CONTROL
         CONTINUE

      ELSEIF (CMD == 'MODEL3') THEN
         !..Set in SCAN_CONTROL
         CONTINUE

      ELSEIF (CMD == 'MODEL4') THEN
         !..Set in SCAN_CONTROL
         CONTINUE

      ELSEIF (CMD(1:5) == 'MODEL') THEN
         IERR = IERR + 1
         IF (NLOG > 0) WRITE (ODBG,10040) &
            'Invalid Model Type ', TRIM (CMD)

!**** Drill and misc parameters ****

      ELSEIF (CMD == 'PI') THEN
         !..Constant "PI"
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            PI_LOC%text, PI_LOC%mat, PI_LOC%prp, &
            PI, IERR)

      ELSEIF (CMD == 'YRSEC') THEN
         !..Year-to-second conversion factor
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            YR2SEC_LOC%text, YR2SEC_LOC%mat, YR2SEC_LOC%prp, &
            XVAL, IERR)
         YR2SEC = DBLE (XVAL)
         YR2SECREAD = .TRUE.

      ELSEIF (CMD == 'SECYR') THEN
         !..Second-to-year conversion factor
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            SEC2YR_LOC%text, SEC2YR_LOC%mat, SEC2YR_LOC%prp, &
            XVAL, IERR)
         !..Convert to year-to-second conversion factor for internal use
         IF (XVAL /= 0.0) THEN
            YR2SEC = 1.0 / DBLE (XVAL)
         END IF
         YR2SECREAD = .FALSE.

      ELSEIF (CMD == 'HREPO') THEN
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            HREPO_LOC%text, HREPO_LOC%mat, HREPO_LOC%prp, &
            HREPO, IERR)

      ELSEIF (CMD == 'COLDIA') THEN
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            COLDIA_LOC%text, COLDIA_LOC%mat, COLDIA_LOC%prp, &
            COLDIA, IERR)

      ELSEIF ((CMD == 'DNSFLUID') .OR. (CMD == 'DENSITY')) THEN
         !..Density for drilling mud
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            DENSITY_LOC%text, DENSITY_LOC%mat, DENSITY_LOC%prp, &
            DENSITY, IERR)

      ELSEIF (CMD == 'DOMEGA') THEN
         !..Angular velocity of drillstring
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            DOMEGA_LOC%text, DOMEGA_LOC%mat, DOMEGA_LOC%prp, &
            DOMEGA, IERR)

      ELSEIF (CMD(1:5) == 'ABSRO') THEN
         !..Absolute borehole roughness
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            ABSRO_LOC%text, ABSRO_LOC%mat, ABSRO_LOC%prp, &
            ABSRO, IERR)

      ELSEIF (CMD == 'TAUFAIL') THEN
         !..Effective shear strength of drilling fluid
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            TAUFAIL_LOC%text, TAUFAIL_LOC%mat, TAUFAIL_LOC%prp, &
            TAUFAIL, IERR)

      ELSEIF (CMD(1:5) == 'VISCO') THEN
         !..Plastic viscosity of drilling fluid
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            VISCO_LOC%text, VISCO_LOC%mat, VISCO_LOC%prp, &
            VISCO, IERR)

      ELSEIF (CMD == 'YLDSTRSS') THEN
         !..Drilling fluid yield stress
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            YLDSTRSS_LOC%text, YLDSTRSS_LOC%mat, YLDSTRSS_LOC%prp, &
            YLDSTRSS, IERR)

      ELSEIF (CMD == 'SHEARRT') THEN
         !..Shear rate
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            SHEARRT_LOC%text, SHEARRT_LOC%mat, SHEARRT_LOC%prp, &
            SHEARRT, IERR)

      ELSEIF (CMD == 'MUDFLWRT') THEN
         !..Drilling mud flow rate
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            MUDFLWRT_LOC%text, MUDFLWRT_LOC%mat, MUDFLWRT_LOC%prp, &
            MUDFLWRT, IERR)

      ELSEIF (CMD == 'DIAMMOD') THEN
         !..Drill diameter - may be read from BRAGFLO CAMDAT variable name
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         IF (WORD .EQ. 'BRAGFLO') THEN
            CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
            ONCDB = .FALSE.
         ELSE
            ONCDB = .TRUE.
         END IF

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, ONCDB, ITYPE, RNUM, WORD, &
            DIAMMOD_LOC%text, DIAMMOD_LOC%mat, DIAMMOD_LOC%prp, &
            XVAL, IERR)

         DO ICAV = 1, NUMCAVS
            DO I = 1, NHITS
               DIAMMOD(I,ICAV,ICURVEC,ICURSCEN) = XVAL
            END DO
         END DO

!**** Model-dependent parameters ****

      ELSEIF (CMD == 'VOLSPALL') THEN
         IF (MODTYPE /= 'MODEL3') THEN
            IF (NLOG > 0) WRITE (ODBG,10010) TRIM (CMD), 'MODEL3'
         END IF
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            VOLSPALL_LOC%text, VOLSPALL_LOC%mat, VOLSPALL_LOC%prp, &
            VOLSPALL, IERR)

      ELSEIF (CMD == 'PTHRESH') THEN
         IF (MODTYPE /= 'MODEL3') THEN
            IF (NLOG > 0) WRITE (ODBG,10010) TRIM (CMD), 'MODEL3'
         END IF
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            PTHRESH_LOC%text, PTHRESH_LOC%mat, PTHRESH_LOC%prp, &
            PTHRESH, IERR)

      ELSEIF (CMD == 'RNDSPALL') THEN
         IF (MODTYPE /= 'MODEL4') THEN
            IF (NLOG > 0) WRITE (ODBG,10010) TRIM (CMD), 'MODEL4'
         END IF
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         !..Assign value directly or from CAMDAT material property
         CALL GETLOCVAL (ODBG, CMD, .TRUE., ITYPE, RNUM, WORD, &
            RNDSPALL_LOC%text, RNDSPALL_LOC%mat, RNDSPALL_LOC%prp, &
            RNDSPALL, IERR)

!**** BRAGFLO CAMDAT names (output CAMDAT names) ****

      ELSEIF (CMD == 'OUT_MAT') THEN
         !..Output CAMDAT material for parameters
         CALL NEXT_C (OUTMAT, IERR)

         !..Check that OUTMAT material exists on input CAMDAT file
         I = IXCDBNAM (OUTMAT, NELBLK, NAMELB)
         IF (I <= 0) THEN
            IERR = IERR + 1
            WRITE (ODBG,10010) &
               'Input CAMDAT file does not contain material ', OUTMAT
         END IF

      ELSEIF ((CMD == 'NAME_GRIDVOL') &
         .OR. (CMD == 'GRIDVOL')) THEN
         !..BRAGFLO CAMDAT attribute name for the gridvol
         CALL NEXT_C (NAMGRIDVOL, IERR)

      ELSEIF ((CMD(1:10) == 'NAME_POROS') &
         .OR. (CMD == 'PORO_NM')) THEN
         !..BRAGFLO CAMDAT variable name for the porosity
         CALL NEXT_C (NAMPOROSITY, IERR)

      ELSEIF ((CMD == 'NAME_PRESGAS') &
         .OR. (CMD == 'PRGS_NM')) THEN
         !..BRAGFLO CAMDAT variable name for the gas pressure
         CALL NEXT_C (NAMPRESGAS, IERR)

      ELSEIF ((CMD == 'NAME_PRESBRIN') &
         .OR. (CMD == 'PRBR_NM')) THEN
         !..BRAGFLO CAMDAT variable name for the brine pressure
         CALL NEXT_C (NAMPRESBRIN, IERR)

      ELSEIF ((CMD == 'NAME_SATGAS') &
         .OR. (CMD == 'SATG_NM')) THEN
         !..BRAGFLO CAMDAT variable name for the gas saturation pressure
         CALL NEXT_C (NAMSATGAS, IERR)

      ELSEIF (CMD == 'NAME_SATBRIN') THEN
         !..BRAGFLO CAMDAT variable name for the brine saturation pressure
         CALL NEXT_C (NAMSATBRIN, IERR)

      ELSEIF ((CMD == 'NAME_PERMBRIN') &
         .OR. (CMD .EQ. 'PRMBX_NM')) THEN
         !..BRAGFLO CAMDAT variable name for the brine permeability
         CALL NEXT_C (NAMPERMBRIN, IERR)

      ELSEIF (CMD(1:9) == 'NAME_BRAG') THEN
         !..BRAGFLO CAMDAT variable name for the user-requested variables
         DO WHILE (.TRUE.)
            CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
            IF (ITYPE /= 0) EXIT
            NBFVAR = NBFVAR + 1
            NAMBFVAR(NBFVAR) = WORD
         END DO

      ELSEIF ((CMD == 'NAME_REPPRES') &
         .OR. (CMD == 'REPPR_NM')) THEN
         !..BRAGFLO CAMDAT variable name for the repository pressure
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
         NAMREPPRES = WORD  ! blank is valid name

!**** Zones for BRAGFLO average variables ****

      ELSEIF (CMD(1:5) == 'INTR_') THEN
         !..Zone for BRAGFLO average variables
         IF (.NOT. BRAGCDB) THEN
            IERR = IERR + 1
            IF (NLOG > 0) WRITE (ODBG,10020) TRIM (CMD)
            GOTO 100
         END IF

         READ (CMD(6:), *) IZONE
         IZONE = IZONE + 1
         IF (IZONE > MAXZONES) THEN
            IERR = IERR + 1
            WRITE (ODBG,10050) &
               'Too many INTR_n commands; max =', MAXZONES
            IZONE = MAXZONES
         END IF

         IXZNCT = IXZNCT + 1
         IF (IXZNCT > MAXALLZONES) THEN
            IERR = IERR + 1
            WRITE (ODBG,10050) &
               'Too many total INTR_n commands; max =', MAXALLZONES
            IXZNCT = MAXALLZONES
         END IF

         IX = IXZNCT
         CALL CMDINTR (ODBG, CMD, MAXZNSETS, ICAV, &
            NELSETS(IX), IELBEG(1,IX), IELEND(1,IX), WTAVG(IX), IERR)
         IF (ICAV == 0) THEN
            DO ICAV = 1, NUMCAVS
               NZONES(ICAV) = MAX (NZONES(ICAV), IZONE)
               IXZONES(IZONE,ICAV) = IX
            END DO
         ELSE IF ((ICAV >= 1) .AND. (ICAV <= NUMCAVS)) THEN
            NZONES(ICAV) = MAX (NZONES(ICAV), IZONE)
            IXZONES(IZONE,ICAV) = IX
         END IF

         IF ((NELSETS(IX) > 0) .AND. (.NOT. WTAVG(IX))) THEN
            IF (NLOG > 0) WRITE (ODBG,10060)
         END IF

      ELSEIF (CMD == 'REPPRES') THEN
         !..Zone for BRAGFLO average repository pressure
         IF (.NOT. BRAGCDB) THEN
            IERR = IERR + 1
            IF (NLOG > 0) WRITE (ODBG,10020) TRIM (CMD)
            GOTO 100
         END IF

         IXZNCT_P = IXZNCT_P + 1
         IF (IXZNCT_P > MAXCAVITIES) THEN
            IERR = IERR + 1
            WRITE (ODBG,10050) &
               'Too many total REPPRES commands; max =', MAXCAVITIES
            IXZNCT_P = MAXCAVITIES
         END IF

         IX = IXZNCT_P
         CALL CMDINTR (ODBG, CMD, MAXZNSETS, ICAV, &
            NELSETS_P(IX), IELBEG_P(1,IX), IELEND_P(1,IX), WTAVG_P(IX), IERR)
         IF (ICAV == 0) THEN
            DO ICAV = 1, NUMCAVS
               IXZONES_P(ICAV) = IX
            END DO
         ELSE IF ((ICAV >= 1) .AND. (ICAV <= NUMCAVS)) THEN
            IXZONES_P(ICAV) = IX
         END IF

         IF ((NELSETS_P(IX) > 0) .AND. (.NOT. WTAVG_P(IX))) THEN
            IF (NLOG > 0) WRITE (ODBG,10060)
         END IF

!**** Test inputs that are normally read from the BRAGFLO CAMDAT file ****

      ELSEIF (CMD == 'POROSITY') THEN
         IF (BRAGCDB) THEN  !This value is read from the BRAGFLO CAMDAT file
            IERR = IERR + 1
            IF (NLOG > 0) WRITE (ODBG,10030) TRIM (CMD)
         END IF

         XVAL = 0.0
         DO I = 0, NHITS
            !..Repeat last value if no more values on line
            CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
            IF ((ITYPE == 1) .OR. (ITYPE == 2)) XVAL = RNUM
            DO ICAV = 1, NUMCAVS
               POROSITY(1,I,ICAV) = XVAL
            END DO
         END DO

      ELSEIF (CMD == 'PRESSURE') THEN
         !..Sets both PRESGAS and REPPRES
         IF (BRAGCDB) THEN  !This value is read from the BRAGFLO CAMDAT file
            IERR = IERR + 1
            IF (NLOG > 0) WRITE (ODBG,10030) TRIM (CMD)
         END IF

         XVAL = 0.0
         DO I = 1, NHITS
            !..Repeat last value if no more values on line
            CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
            IF ((ITYPE == 1) .OR. (ITYPE == 2)) XVAL = RNUM
            DO ICAV = 1, NUMCAVS
               PRESGAS(1,I,ICAV) = XVAL
               REPPRES(I,ICAV,ICURVEC,ICURSCEN) = XVAL
            END DO
         END DO

      ELSE
         !..Meaningless data
         IERR = IERR + 1
         IF (NLOG > 0) WRITE (ODBG,10040) 'Unknown keyword ', TRIM (CMD)
      END IF

      !..Scan next record
      GOTO 100

  110 CONTINUE

      IF (IERR > 0) CALL QAABORT &
         ('Reading input control file')

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/, &
         1X,'Reading input control file',/,4X,A,/)
10010 FORMAT (' *** WARNING - ', A, ' ignored if not ', A)
10020 FORMAT (' *** ERROR - ', A, ' ignored if no BRAGFLO CAMDAT file input')
10030 FORMAT (' *** ERROR - ', A, ' ignored if BRAGFLO CAMDAT file input')
10040 FORMAT (' *** ERROR - ', 5A)
10050 FORMAT (' *** ERROR - ', A, I4)
10060 FORMAT (' >>> Simple average will be used;', &
         ' average will not be weighted by grid volume')

      END SUBROUTINE READ_CONTROL


      SUBROUTINE CMDINTR (ODBG, CMD, &
         MAXZNSETS, ICAV, NELSETS, IELBEG, IELEND, WTAVG, IERR)
!***********************************************************************
!
!  PURPOSE:     Processes the INTR_n or REPPRES command from the input control
!               file.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS;
!                              Derived from part of REATXT1
!
!  CALLED BY:   READ_CONTROL
!
!  ARGUMENTS:   None
!     ODBG      input  Output debug file
!     CMD       input  Command verb (for error messages)
!     MAXZNSETS input  Maximum number of zone sets
!     ICAV      output Cavity to which zone sets applies (0 for all)
!     NELSETS   output Number of zone sets
!     IELBEG    output The beginning element number for the zone sets
!     IELEND    output The ending element number for the zone sets
!     WTAVG     output True iff weighted average requested
!     IERR      in/out Error flag; increment if error
!
!***********************************************************************

      USE BRAG_SIZING_MODULE
      USE SCEN_INFO_MODULE

      IMPLICIT NONE

      INTEGER ODBG
      CHARACTER*(*) CMD
      INTEGER MAXZNSETS
      INTEGER ICAV
      INTEGER NELSETS
      INTEGER IELBEG(*)
      INTEGER IELEND(*)
      LOGICAL WTAVG
      INTEGER IERR

      INTEGER I, IBEG, IEND, IELB, IERRX
      INTEGER ITYPE, INUM
      DOUBLE PRECISION RNUM  !apg REAL
      CHARACTER*18 WORD

! Function definitions
      INTEGER IXCDBNAM

      NELSETS = 0

      CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)
      IF (WORD == '*') THEN
         ICAV = 0
      ELSE
         DO ICAV = 1, NUMCAVS
            IF (WORD == CAVABBR(ICAV)) EXIT
         END DO
         IF (ICAV > NUMCAVS) THEN
            IERR = IERR + 1
            WRITE (ODBG,10010) 'Expected cavity abbreviation or "*"'
            ICAV = -1
            RETURN
         END IF
      END IF

      NELSETS = 0
      WTAVG = .TRUE.

      DO WHILE (.TRUE.)
         CALL NEXTWORD (ITYPE, WORD, INUM, RNUM)

         IF ((ITYPE < 0) .AND. (NELSETS > 0)) EXIT

         IF ((ITYPE == 0) .AND. (WORD == 'NO_GRDVOL')) THEN
            WTAVG  = .FALSE.
            CYCLE
         END IF

         IF ((ITYPE == 0) .AND. (WORD == 'TO')) THEN
            CALL NEXT_I (INUM, IERRX)
            IF (IERRX /= 0) THEN
               IERR = IERR + 1
               RETURN
            END IF
            IF (NELSETS < 1) THEN
               IERR = IERR + 1
               WRITE (ODBG,10010) 'TO must follow element number'
               RETURN
            END IF
            IELEND(NELSETS) = INUM
            CYCLE

         ELSE IF (ITYPE == 0) THEN
            !..Get BRAGFLO CAMDAT material index
            IELB = IXCDBNAM (WORD, NELBLK_B, NAMELB_B)
            IF (IELB <= 0) THEN
               IERR = IERR + 1
               IF (ODBG > 0) WRITE (ODBG,10000) TRIM (CMD), TRIM (WORD)
               CYCLE
            END IF

            !..Calculate starting and ending elements for selected block
            IF (IELB > 0) THEN
               IBEG = 0
               DO I = 1, IELB-1
                  IBEG = IBEG + NUMLNK_B(I)
               END DO
               IEND = IBEG + NUMLNK_B(IELB)
               IBEG = IBEG + 1
            END IF

         ELSE IF (ITYPE == 2) THEN
            !..If single element is requested, assign indices and return
            IBEG = INUM
            IEND = INUM
            IF (NELSETS > 0) THEN
               IF (IBEG == IELEND(NELSETS)+1) THEN
                  IELEND(NELSETS) = IEND
                  CYCLE
               END IF
            END IF
         END IF

         !..Increment number of zone sets
         NELSETS = NELSETS + 1
         IF (NELSETS >= MAXZNSETS) THEN
            IERR = IERR + 1
            WRITE (ODBG,10020) &
               'Too many zone elements specified; max =', MAXZNSETS
            NELSETS = MAXZNSETS
         END IF
         IELBEG(NELSETS) = IBEG
         IELEND(NELSETS) = IEND
      END DO

      RETURN
10000 FORMAT (' *** ERROR - ', A, ' Material ', A, &
         ' is not on BRAGFLO CAMDAT file')
10010 FORMAT (' *** ERROR - ', 5A)
10020 FORMAT (' *** ERROR - ', A, I4)
      END


      SUBROUTINE GETLOCVAL (ODBG, CMD, ONCDB, ITYPE, VARVAL, VARSTR, &
         LOCTXT, MATNAM, PRPNAM, XVAL, IERR)
!***********************************************************************
!
!  PURPOSE:     Gets a value that may be input directly or read from a
!               CAMDAT property.  For a CAMDAT property, only the location
!               (material, property) is returned.
!
!               The CAMDAT property is specified by the material name
!               concatenated with a colon (":") and the property name
!               (e.g., "MATERIAL_NAME:PROPERTY_NAME").
!
!  AUTHOR:      Jonathan S. Rath
!
!  UPDATED:     xx-SEP-2004  --APG: Rewrite of GETVAL;
!                              Value may be given directly;
!                              CAMDAT property location returned
!               04-NOV-1993  --JSR: First Ed. for CUTTINGS
!
!  CALLED BY:   READ_CONTROL
!
!  ARGUMENTS:
!     ODBG      input  File for error and info messages; if 0, no output
!     CMD       input  Name of value being processed, for message only
!     ONCDB     input  True if property is from input CAMDAT, false if from
!                      BRAGFLO CAMDAT
!     ITYPE     input  Type of input value from free-field reader
!     VARVAL    input  Value, if TYPE=1 or 2
!     VARSTR    input  String, if TYPE=0, with material:property
!     LOCTXT    in/out Text describing location (CAMDAT, BRAGFLO, Value)
!     MATNAM    output Material name, if any
!     PRPNAM    output Property name, if any
!     XVAL      output Returned value, if value given
!     IERR      in/out Error flag; increment if error
!
!***********************************************************************

      USE CDB_PROPS_MODULE
      USE BRAG_SIZING_MODULE

      IMPLICIT NONE

      INTEGER ODBG
      CHARACTER*(*) CMD
      LOGICAL ONCDB
      INTEGER ITYPE
      DOUBLE PRECISION VARVAL  !apg REAL
      CHARACTER*(*) VARSTR
      CHARACTER*(*) LOCTXT
      CHARACTER*(*) MATNAM, PRPNAM
      DOUBLE PRECISION XVAL  !apg REAL
      INTEGER IERR

      INTEGER ICLN, IMAT, IPRP

! Function definitions
      INTEGER IXCDBNAM

!**** EXECUTABLE STATEMENTS ****

      XVAL = 0.0

      IF ((ITYPE == 1) .OR. (ITYPE == 2)) THEN
         !..Assign numeric value given, and return
         LOCTXT = 'Value'
         XVAL = VARVAL
         RETURN

      ELSEIF ((ITYPE == 0) .AND. ONCDB) THEN
         !..Assign value from CAMDAT material property

         !..Get material name and property name from VARSTR
         ICLN = INDEX (VARSTR, ':')
         IF (ICLN <= 1) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10000) TRIM (CMD), TRIM (VARSTR)
            RETURN
         END IF
         LOCTXT = 'CAMDAT'
         MATNAM = VARSTR(:ICLN-1)
         PRPNAM = VARSTR(ICLN+1:17)

         !..Get material index
         IMAT = IXCDBNAM (MATNAM, NELBLK, NAMELB)
         IF (IMAT <= 0) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10010) TRIM (CMD), &
               TRIM (VARSTR), TRIM (MATNAM)
         END IF

         !..Get property index
         IPRP = IXCDBNAM (PRPNAM, NUQPRP, NMATPR)
         IF (IPRP <= 0) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10020) TRIM (CMD), &
               TRIM (VARSTR), TRIM (PRPNAM)
         END IF

      ELSEIF ((ITYPE == 0) .AND. (.NOT. ONCDB)) THEN
         !..Assign value from BRAGFLO CAMDAT material property

         !..Get material name and property name from VARSTR
         ICLN = INDEX (VARSTR, ':')
         IF (ICLN <= 1) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10000) TRIM (CMD), TRIM (VARSTR)
            RETURN
         END IF
         LOCTXT = 'BRAGFLO'
         MATNAM = VARSTR(:ICLN-1)
         PRPNAM = VARSTR(ICLN+1:17)

         !..Get material index
         IMAT = IXCDBNAM (MATNAM, NELBLK_B, NAMELB_B)
         IF (IMAT <= 0) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10030) TRIM (CMD), &
               TRIM (VARSTR), TRIM (MATNAM)
         END IF

         !..Get property index
         IPRP = IXCDBNAM (PRPNAM, NUQPRP_B, NMATPR_B)
         IF (IPRP <= 0) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10040) TRIM (CMD), &
               TRIM (VARSTR), TRIM (PRPNAM)
         END IF

      ELSE
         !..Error, return
         IERR = IERR + 1
         IF (ODBG > 0) WRITE (ODBG,10050) TRIM (CMD), TRIM (VARSTR)
         RETURN
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (' *** ERROR - Invalid ', A, ' material:property ', A, &
         ' - no colon between material name and property name')
10010 FORMAT (' *** ERROR - Invalid ', A, ' material:property ', A, &
         ' - material ', A, ' is not on input CAMDAT file')
10020 FORMAT (' *** ERROR - Invalid ', A, ' material:property ', A, &
         ' - property ', A, ' is not on input CAMDAT file')
10030 FORMAT (' *** ERROR - Invalid ', A, ' material:property ', A, &
         ' - material ', A, ' is not on BRAGFLO CAMDAT file')
10040 FORMAT (' *** ERROR - Invalid ', A, ' material:property ', A, &
         ' - property ', A, ' is not on BRAGFLO CAMDAT file')
10050 FORMAT (' *** ERROR - Expected ', A, &
         ' value or material:property, not ', A)

      END SUBROUTINE GETLOCVAL


      SUBROUTINE VECTOR_CONTROL (FICDB)
!***********************************************************************
!
!  PURPOSE:     Sets the parameters that are read from the input CAMDAT
!               file.  The parameter locations, if any, are stored when
!               the input control file is read.  Writes the value and origin
!               of each parameter to debug file.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS
!
!  CALLED BY:   CUTTINGS_S (main program)
!
!  ARGUMENTS:
!     FICDB     input  Filename of input CAMDAT file
!
!  Sets parameters in CONSTANTS_MODULE
!  Sets parameters in DRILL_PARAMS_MODULE
!  Sets parameters in MODEL_PARAMS_MODULE
!  Sets DIAMMOD in OUTPUT_DATA_MODULE
!
!***********************************************************************

      USE CONSTANTS_MODULE
      USE DEBUG_MODULE
      USE DRILL_PARAMS_MODULE
      USE MODEL_PARAMS_MODULE
      USE OUTPUT_DATA_MODULE
      USE SCEN_INFO_MODULE
      USE PARAM_LOCS_MODULE

      IMPLICIT NONE

      CHARACTER*(*) FICDB

      LOGICAL WRT
      INTEGER I, ICAV, IERR, NLOG
      DOUBLE PRECISION XVAL  !apg REAL
      CHARACTER*18 CMD, WORD

!**** EXECUTABLE STATEMENTS ****

      IERR = 0

      WRITE (ODBG,10000) TRIM (FICDB)

!**** Drill and misc parameters ****

      CMD = 'PI'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         PI_LOC%text, PI_LOC%mat, PI_LOC%prp, PI, IERR)

      IF (YR2SECREAD) THEN
         CMD = 'YRSEC'
         XVAL = YR2SEC  ! YR2SEC is double precision, routine needs single
         CALL GETVECVAL (ODBG, .TRUE., CMD, &
            YR2SEC_LOC%text, YR2SEC_LOC%mat, YR2SEC_LOC%prp, XVAL, IERR)
         IF (YR2SEC_LOC%text == 'CAMDAT') THEN
            YR2SEC = DBLE (XVAL)
         END IF
      ELSE
         CMD = 'SECYR'
         XVAL = 1.0 / YR2SEC  ! YR2SEC is double precision, routine needs single
         CALL GETVECVAL (ODBG, .TRUE., CMD, &
            SEC2YR_LOC%text, SEC2YR_LOC%mat, SEC2YR_LOC%prp, XVAL, IERR)
         IF (YR2SEC_LOC%text == 'CAMDAT') THEN
            YR2SEC = 1.0 / DBLE (XVAL)
         END IF
      END IF

      CMD = 'HREPO'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         HREPO_LOC%text, HREPO_LOC%mat, HREPO_LOC%prp, HREPO, IERR)

      CMD = 'COLDIA'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         COLDIA_LOC%text, COLDIA_LOC%mat, COLDIA_LOC%prp, COLDIA, IERR)

      CMD = 'DNSFLUID'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         DENSITY_LOC%text, DENSITY_LOC%mat, DENSITY_LOC%prp, DENSITY, IERR)

      CMD = 'DOMEGA'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         DOMEGA_LOC%text, DOMEGA_LOC%mat, DOMEGA_LOC%prp, DOMEGA, IERR)

      CMD = 'ABSROUGH'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         ABSRO_LOC%text, ABSRO_LOC%mat, ABSRO_LOC%prp, ABSRO, IERR)

      CMD = 'TAUFAIL'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         TAUFAIL_LOC%text, TAUFAIL_LOC%mat, TAUFAIL_LOC%prp, TAUFAIL, IERR)

      CMD = 'VISCO'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         VISCO_LOC%text, VISCO_LOC%mat, VISCO_LOC%prp, VISCO, IERR)

      CMD = 'YLDSTRSS'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         YLDSTRSS_LOC%text, YLDSTRSS_LOC%mat, YLDSTRSS_LOC%prp, YLDSTRSS, IERR)

      CMD = 'SHEARRT'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         SHEARRT_LOC%text, SHEARRT_LOC%mat, SHEARRT_LOC%prp, SHEARRT, IERR)

      CMD = 'MUDFLWRT'
      CALL GETVECVAL (ODBG, .TRUE., CMD,&
         MUDFLWRT_LOC%text, MUDFLWRT_LOC%mat, MUDFLWRT_LOC%prp, MUDFLWRT, IERR)

      WRT = .TRUE.
      CMD = 'DIAMMOD'
      IF (DIAMMOD_LOC%text == 'BRAGFLO') THEN
         XVAL = 0.0
      ELSE IF (ICURVEC <= 1) THEN
         XVAL = DIAMMOD(1,1,ICURVEC,ICURSCEN)
      ELSE
         XVAL = DIAMMOD(1,1,ICURVEC-1,ICURSCEN)
      END IF
      CALL GETVECVAL (ODBG, WRT, CMD, &
         DIAMMOD_LOC%text, DIAMMOD_LOC%mat, DIAMMOD_LOC%prp, XVAL, IERR)
      IF (DIAMMOD_LOC%text == 'CAMDAT') THEN
         DO ICAV = 1, NUMCAVS
            DO I = 1, NHITS
               DIAMMOD(I,ICAV,ICURVEC,ICURSCEN) = XVAL
            END DO
         END DO
      END IF

!**** Model-dependent parameters ****

      WRT = (MODTYPE == 'MODEL3')
      CMD = 'VOLSPALL'
      CALL GETVECVAL (ODBG, WRT, CMD, &
         VOLSPALL_LOC%text, VOLSPALL_LOC%mat, VOLSPALL_LOC%prp, VOLSPALL, IERR)

      WRT = (MODTYPE == 'MODEL3')
      CMD = 'PTHRESH'
      CALL GETVECVAL (ODBG, WRT, CMD, &
         PTHRESH_LOC%text, PTHRESH_LOC%mat, PTHRESH_LOC%prp, PTHRESH, IERR)

      WRT = (MODTYPE == 'MODEL4') .AND. ((RNDSPALL_LOC%text == 'CAMDAT') &
         .OR. (RNDSPALL_LOC%text == 'BRAGFLO'))
      CMD = 'RNDSPALL'
      CALL GETVECVAL (ODBG, WRT, CMD, &
         RNDSPALL_LOC%text, RNDSPALL_LOC%mat, RNDSPALL_LOC%prp, RNDSPALL, IERR)

      IF (IERR > 0) CALL QAABORT &
         ('Retrieving vector parameters from input CAMDAT file')

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/, &
         1X,'Retrieving vector parameters from input CAMDAT file',/,4X,A,/)

      END SUBROUTINE VECTOR_CONTROL


      SUBROUTINE GETVECVAL (ODBG, WRT, CMD, LOCTXT, MATNAM, PRPNAM, XVAL, IERR)
!***********************************************************************
!
!  PURPOSE:     Gets the value, if it is an input CAMDAT property (else value
!               unchanged).  Writes the value and its origin, if requested.
!
!  AUTHOR:      Amy P. Gilkey
!
!  UPDATED:     xx-SEP-2004  --APG: First Edition for CUTTINGS
!
!  CALLED BY:   VECTOR_CONTROL
!
!  ARGUMENTS:
!     ODBG      input  File for error and info messages; if 0, no output
!     WRT       input  Write the parameter value if TRUE
!     CMD       input  Name of value being processed, for message only
!     LOCTXT    input  Text describing location (CAMDAT, Value)
!     MATNAM    input  Material name
!     PRPNAM    input  Property name
!     XVAL      in/out Returned value; only set if MATNAM not blank
!     IERR      in/out Error flag; increment if error
!
!***********************************************************************

      USE CDB_PROPS_MODULE

      IMPLICIT NONE

      INTEGER ODBG
      LOGICAL WRT
      CHARACTER*(*) CMD
      CHARACTER*(*) LOCTXT
      CHARACTER*(*) MATNAM, PRPNAM
      DOUBLE PRECISION XVAL  !apg REAL
      INTEGER IERR

      INTEGER ICLN, IMAT, IPRP

! Function definitions
      INTEGER IXCDBNAM

!**** EXECUTABLE STATEMENTS ****

      IF (LOCTXT == 'CAMDAT') THEN

         !..Get material index
         IMAT = IXCDBNAM (MATNAM, NELBLK, NAMELB)
         IF (IMAT <= 0) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10000) TRIM (CMD), TRIM (MATNAM)
         END IF

         !..Get property index
         IPRP = IXCDBNAM (PRPNAM, NUQPRP, NMATPR)
         IF (IPRP <= 0) THEN
            IERR = IERR + 1
            IF (ODBG > 0) WRITE (ODBG,10010) TRIM (CMD), TRIM (PRPNAM)
         END IF

         IF ((IMAT > 0) .AND. (IPRP > 0)) THEN
            IF (IASPRP(IMAT,IPRP)) THEN
               XVAL = XMATPR(IMAT,IPRP)
               IF (WRT) WRITE (ODBG,10030) CMD(:12), XVAL, MATNAM, PRPNAM
            ELSE
               IERR = IERR + 1
               IF (ODBG > 0) WRITE (ODBG,10020) TRIM (CMD), &
                  TRIM (PRPNAM), TRIM (MATNAM)
            END IF
         ELSE
            IF (IERR == 0) IERR = IERR + 1
         END IF

      ELSE IF (LOCTXT == 'BRAGFLO') THEN
         !..Value is not set
         IF (WRT) WRITE (ODBG,10050) CMD, LOCTXT

      ELSE
         !..Value remains unchanged
         IF (WRT) WRITE (ODBG,10040) CMD, XVAL, LOCTXT
      END IF

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (' *** ERROR - Invalid ', A, &
         ' - material ', A, ' is not on input CAMDAT file')
10010 FORMAT (' *** ERROR - Invalid ', A, &
         ' - property ', A, ' is not on input CAMDAT file')
10020 FORMAT (' *** ERROR - Invalid ', A, &
         ' - property ', A, ' is not defined for material ', A)
10030 FORMAT (1X, A12, 1PE14.6, 3X, 'CAMDAT material:property ', A,':',A)
10040 FORMAT (1X, A12, 1PE14.6, 3X, A)
10050 FORMAT (1X, A12, 14X,     3X, A)

      END SUBROUTINE GETVECVAL
