      MODULE CONSTANTS_MODULE
!***********************************************************************
!
!  PURPOSE:     Contains mathematical constants.  These "constants" may
!               be read from the input control file.
!
!  UPDATED:     xx-SEP-2004  --APG: Created from CONSTA.INC
!
!***********************************************************************

      DOUBLE PRECISION PI  !apg REAL
      ! PI = Common mathematical constant "PI"
      DOUBLE PRECISION YR2SEC
      ! YR2SEC = Years-to-seconds conversion factor

      END MODULE CONSTANTS_MODULE
