      SUBROUTINE DRILL (XDRILDIA, XCOLDIA, XAREAC)
!***********************************************************************
!
!  PURPOSE:     Calculates new borehole area based on drilling and
!               penetrating either:
!               (1) WIPP repository containing CH or RH type
!                   transuranic waste, or
!               (2) Hypothetical INEL/WINCO repository containing
!                   HLW/SNF waste.
!
!  AUTHORS:     Jerry W. Berglund
!               Jonathan S. Rath
!
!  UPDATED:     08-JUN-2005  --EDV: Added IF statement for when
!				    ROUTER<RINNER
!		xx-SEP-2004  --APG: Cosmetic changes;
!                              Isolated SIMPSONS into new routine;
!                              Moved some of the K=1,2 IFs
!               25-AUG-1994  --JWB: Change flowrate constant from
!                                   9.925e-2 to 9.935e-2
!               29-MAR-1994  --JSR: Added more WRITE statements.
!                                   Added CC=0.0 at beginning.
!               17-FEB-1994  --JSR: Adapted to CUTTINGS C-3.10ZO
!               03-NOV-1993  --JSR: Placed original "drill" program
!                                   of CUTTINGS code in a separate
!                                   subroutine called DRILL.
!                                   AINT(*) array not used anymore.
!
!  CALLED BY:   CUTSPALL
!
!  ARGUMENTS:
!      XDRILDIA input  Drill Bit Diameter for this intrusion
!                      (Outside diameter of drillstring annulus)
!      XCOLDIA  input  Collar diameter (m)
!      XAREAC   output Resultant borehole area from drilling through
!                      repository and removing cuttings
!
!  Initializes DRILL_INTERNALS_MODULE
!
!***********************************************************************

      USE CONSTANTS_MODULE
      USE DRILL_INTERNALS_MODULE
      USE DRILL_PARAMS_MODULE
      USE DEBUG_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION XDRILDIA, XCOLDIA  !apg REAL
      DOUBLE PRECISION XAREAC  !apg REAL

      INTEGER K, MM
      DOUBLE PRECISION CC, DT2DR, RDIF, ROLD, RORIG, RT, &  !apg REAL
         TAURT, TAURZ, TAU2, TAU2OLD, TREY, &
         VBAR, VEL

! Function definitions
      DOUBLE PRECISION FRICTION  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      WRITE (ODBG,10000)

      !..Zero out DRILL Internals for SIMPSONS
      LAMBDA2 = 1.0
      C = 1.0
      RJ2 = 1.0

      !..Initialize
      CC = 0.0
      MM = 0
      TAU2 = 0.0

      !..Calculate flowrate based on Austin, E.H.,
      !..   "Drilling Engineering Handbook", 1983
      !..   0.09935 m**3/(s*m)
      FLOWRATE = XDRILDIA * MUDFLWRT

      !..Calculate Oldroyd parameters based on Bingham data

      ETA0 = 2.0*VISCO
      SIGMA2 = (SHEARRT*VISCO-YLDSTRSS) / (2.0*SHEARRT*SHEARRT*YLDSTRSS)
      SIGMA1 = 2.0*SIGMA2

      WRITE (ODBG,10010) &
         DOMEGA, DENSITY, TAUFAIL, ABSRO, VISCO, YLDSTRSS, &
         XDRILDIA, XCOLDIA, &
         FLOWRATE, ETA0, SIGMA1, SIGMA2

      !..Set drill radius and drill collar radius

      ROUTER = XDRILDIA / 2.0
      RINNER = XCOLDIA / 2.0
      RORIG = ROUTER

      !..Calculate Reynolds number

      VBAR = FLOWRATE/PI/(ROUTER**2-RINNER**2)  !!! OR flowrate / (pi*(...))
      TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0

      IF (TREY >= 2100.0) THEN
         WRITE (ODBG,10020) TREY, 'TURBULENT'
      ELSE
         WRITE (ODBG,10020) TREY, 'LAMINAR'
      END IF

      !..Calculate critical radius

      RT = DENSITY*FLOWRATE/(643.0*PI*ETA0) - RINNER

      !..Calculate the multiplier CC of VBAR required to match
      !..the eroded radius at TREY=2100 for the turbulent and
      !..laminar solutions for non-zero DOMEGA.  This process
      !..accounts for drillstring rotation in the turbulent
      !..flow regime.

      IF (TREY >= 2100.0) THEN
         CALL INTERFACE (CC)
         WRITE (ODBG,10030) CC
      END IF

      WRITE (ODBG,10040)
!!!      TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
      IF (TREY >= 2100.0) GOTO 120

!***********************************************************************
      !..Flow is LAMINAR
!***********************************************************************

  100 CONTINUE
  110 CONTINUE
      ROLD = ROUTER
      DO K = 1, 2
         IF (K == 2) ROUTER = 1.0001*ROUTER

         !..Perform numerical integration of coefficients
         !..using Simpson's 1/3 rule
         !..Solve linear equations for corrections
         !..Calculate the wall shear stresses

         CALL SIMPSONS (TAURT, TAURZ)

         !..Calculate radius at which shear stress equals
         !..failure shear stress

         TAU2OLD = TAU2
         TAU2 = TAURT*TAURT + TAURZ*TAURZ  !!! TAU2 is sqrt(x) in INTERFACE
      END DO
      DT2DR = -(TAU2-TAU2OLD)/0.0001/ROUTER
      ROUTER = ROLD

      ROUTER = ROUTER + (TAU2OLD-TAUFAIL**2) / DT2DR
      IF (ROUTER < 0.0) THEN
         ROUTER = 1.1*RINNER
         MM = MM+1
         IF (MM > 2) THEN
            WRITE (ODBG,10070) 'NEGATIVE ROUTER computed' &
               //' in Laminar-Helical flow algorithm'
            WRITE (ODBG,10080) &
               'TAURT', TAURT, 'TAURZ', TAURZ, &
               'TAU2OLD', TAU2OLD, 'TAU2', TAU2
            WRITE (ODBG,10080) &
               'ROLD', ROLD, 'RINNER', RINNER, 'ROUTER', ROUTER
            CALL QAABORT ('Negative ROUTER in DRILL')
         END IF
         GOTO 110
      END IF
      RDIF = ROUTER-ROLD
      IF (ABS(RDIF) > 0.0001*RORIG) GOTO 110

      !..Calculate area of repository material cut and eroded

      IF (ROUTER < RORIG) ROUTER = RORIG

      XAREAC = PI*ROUTER**2

      VBAR = FLOWRATE/(PI*(ROUTER**2-RINNER**2))
      TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
      WRITE (ODBG,10050) RINNER, RORIG, ROUTER, RT, &
         XAREAC, TREY, 'LAMINAR'

      IF (TREY < 2100.0) GOTO 140

!***********************************************************************
      !..Flow is TURBULENT
!***********************************************************************

  120 CONTINUE
  130 CONTINUE
      ROLD = ROUTER
      DO K = 1, 2
         IF (K == 2) ROUTER = 1.001*ROUTER
         VBAR = FLOWRATE/(PI*(ROUTER**2-RINNER**2))
         TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
         VEL = CC*VBAR
         TAU2OLD = TAU2
         TAU2 = (0.5*FRICTION(TREY)*DENSITY*VEL**2/0.8165)**2
      END DO
      DT2DR = -(TAU2-TAU2OLD)/0.001/ROUTER
      ROUTER = ROLD

      !..Calculate radius at which shear stress equals
      !..failure shear stress

      ROUTER = ROUTER + (TAU2OLD-TAUFAIL**2) / DT2DR

      IF (RINNER < ROUTER) THEN
         RDIF = ROUTER-ROLD
         IF (ABS(RDIF) > 0.0001*RORIG) GOTO 130
         IF (ROUTER >= RT) THEN
            ROUTER = RT
            WRITE (ODBG,10060)
            GOTO 100
         END IF
      ELSE
         ROUTER = RORIG
      END IF

      !..Calculate area of repository material cut and eroded

      IF (ROUTER < RORIG) ROUTER = RORIG

      XAREAC = PI*ROUTER**2

      VBAR = FLOWRATE/(PI*(ROUTER**2-RINNER**2))  !!! added VBAR for TREY
      TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
      WRITE (ODBG,10050) RINNER, RORIG, ROUTER, RT, &
         XAREAC, TREY, 'TURBULENT'

  140 CONTINUE

      RETURN

!**** FORMAT STATEMENTS ****
10000 FORMAT (/,1X,79('*'),/, &
         1X,'Calculating Cuttings and Cavings',/)
10010 FORMAT ( &
         1X,'DOMEGA   =',1PE12.4, 3X, &
         1X,'DENSITY  =',1PE12.4, 3X, &
         1X,'TAUFAIL  =',1PE12.4, /, &
         1X,'ABSRO    =',1PE12.4, 3X, &
         1X,'VISCO    =',1PE12.4, 3X, &
         1X,'YLDSTRSS =',1PE12.4, /, &
         1X,'DRILDIA  =',1PE12.4, 3X, &
         1X,'COLDIA   =',1PE12.4, 3X, &
         1X,'FLOWRATE =',1PE12.4, /, &
         1X,'ETA0     =',1PE12.4, 3X, &
         1X,'SIGMA1   =',1PE12.4, 3X, &
         1X,'SIGMA2   =',1PE12.4)
10020 FORMAT (/,' Initial Reynolds Number = ',F10.0,', Flow is ', A, / &
         4X,'If >=2100 assumes TURBULENT AXIAL Flow;', &
         ' <2100 assumes LAMINAR HELICAL Flow')
10030 FORMAT (/,' VBAR Multiplier (CC) =',1PE12.4)
10040 FORMAT (/, &
         ' Collar     Drill      Final      Critical   ', &
         'Final      Reynolds   Solution  ', /, &
         ' Radius     Radius     Radius     Radius     ', &
         'Area       Number     Algorithm ', /, &
         ' ---------- ---------- ---------- ---------- ', &
         '---------- ---------- ----------')
10050 FORMAT (1P6E11.4,1X,A)
10060 FORMAT (/,' *** Checking for LAMINAR FLOW SOLUTION ***')
10070 FORMAT (/, 1X,72('#'), /, 1X,'### ',A,' ###', /)
10080 FORMAT (1X,4(A,' =',1PE11.4,3X))

      END SUBROUTINE DRILL


      SUBROUTINE SIMPSONS (TAURT, TAURZ)
!***********************************************************************
!
!  PURPOSE:     Performs numerical integration of coefficients using
!               Simpson's 1/3 rule
!
!  AUTHOR:      Jerry W. Berglund
!
!  UPDATED:     xx-SEP-2004  --APG: Created by extracting code from DRILL
!                              and INTERFACE;
!                              EFx and Fxx calls combined into single routine
!
!  CALLED BY:   DRILL and INTERFACE
!
!  ARGUMENTS:
!      TAURT    output Wall shear stress
!      TAURZ    output Wall shear stress
!
!  Sets DRILL_INTERNALS_MODULE
!
!***********************************************************************

      USE CONSTANTS_MODULE
      USE DRILL_INTERNALS_MODULE
      USE DRILL_PARAMS_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION TAURT, TAURZ  !apg REAL

      INTEGER I, J
      DOUBLE PRECISION ALPHA, RHO  !apg REAL
      DOUBLE PRECISION A11, A12, A13, A21, A22, A23, A31, A32, A33, &  !apg REAL
         F11, F12, F13, F21, F22, F23, F31, F32, F33
      DOUBLE PRECISION EF1, EF2, EF3, F1, F2, F3  !apg REAL
      DOUBLE PRECISION DEL  !apg REAL
      DOUBLE PRECISION DETER, DX, DY, DZ, DL2, DC, DRJ2  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      ALPHA = RINNER/ROUTER

      DO J = 1, 25

         !..Numerical integration of coefficients using Simpson's 1/3 rule

         A11 = 0.0
         A12 = 0.0
         A13 = 0.0
         A21 = 0.0
         A22 = 0.0
         A23 = 0.0
         A31 = 0.0
         A32 = 0.0
         A33 = 0.0
         F1 = 0.0
         F2 = 0.0
         F3 = 0.0

         DO I = 1, 2
            RHO = (1.0-ALPHA)*I + 2.0*ALPHA - 1.0
            CALL FXX (ALPHA, RHO, &
               F11, F12, F13, F21, F22, F23, F31, F32, F33, &
               EF1, EF2, EF3)
            A11 = A11 + F11
            A12 = A12 + F12
            A13 = A13 + F13
            A21 = A21 + F21
            A22 = A22 + F22
            A23 = A23 + F23
            A31 = A31 + F31
            A32 = A32 + F32
            A33 = A33 + F33
            F1 = F1 + EF1
            F2 = F2 + EF2
            F3 = F3 + EF3
         END DO

         DO I = 2, 10, 2
            RHO = ALPHA + (1.0-ALPHA)/10.0*(I-1)
            CALL FXX (ALPHA, RHO, &
               F11, F12, F13, F21, F22, F23, F31, F32, F33, &
               EF1, EF2, EF3)
            A11 = A11 + F11*4.
            A12 = A12 + F12*4.
            A13 = A13 + F13*4.
            A21 = A21 + F21*4.
            A22 = A22 + F22*4.
            A23 = A23 + F23*4.
            A31 = A31 + F31*4.
            A32 = A32 + F32*4.
            A33 = A33 + F33*4.
            F1 = F1 + EF1*4.
            F2 = F2 + EF2*4.
            F3 = F3 + EF3*4.
         END DO

         DO I = 3, 9, 2
            RHO = ALPHA + (1.0-ALPHA)/10.0*(I-1)
            CALL FXX (ALPHA, RHO, &
               F11, F12, F13, F21, F22, F23, F31, F32, F33, &
               EF1, EF2, EF3)
            A11 = A11 + F11*2.
            A12 = A12 + F12*2.
            A13 = A13 + F13*2.
            A21 = A21 + F21*2.
            A22 = A22 + F22*2.
            A23 = A23 + F23*2.
            A31 = A31 + F31*2.
            A32 = A32 + F32*2.
            A33 = A33 + F33*2.
            F1 = F1 + EF1*2.
            F2 = F2 + EF2*2.
            F3 = F3 + EF3*2.
         END DO

         DEL = (1.0-ALPHA)/30.0
         A11 = A11*DEL
         A12 = A12*DEL
         A13 = A13*DEL
         A21 = A21*DEL
         A22 = A22*DEL
         A23 = A23*DEL
         A31 = A31*DEL
         A32 = A32*DEL
         A33 = A33*DEL
         F1 = F1*DEL
         F2 = F2*DEL
         F3 = F3*DEL
         F3 = F3 + 4.*FLOWRATE/PI/ROUTER**3
         F2 = F2 - DOMEGA

         !..End of integration

         !..Solve linear equations for corrections

         DETER = A11*A22*A33 + A12*A23*A31 + A13*A21*A32 &
            - A13*A22*A31 - A32*A23*A11 - A33*A21*A12
         DX = -F1*A22*A33 - F3*A12*A23 - F2*A13*A32 &
            + F1*A23*A32 + F2*A33*A12 + F3*A22*A13
         DY = -F2*A11*A33 - F1*A23*A31 - F3*A13*A21 &
            + F2*A31*A13 + F3*A23*A11 + F1*A33*A21
         DZ = -F3*A11*A22 - F2*A12*A31 - F1*A21*A32 &
            + F1*A22*A31 + F2*A11*A32 + F3*A21*A12
         DL2  = DX/DETER
         DC   = DY/DETER
         DRJ2 = DZ/DETER
         LAMBDA2 = LAMBDA2 + DL2
         C       = C + DC
         RJ2     = RJ2 + DRJ2

         !..Convergence criterion

         IF (     ABS(F1) < 0.00001 &
            .AND. ABS(F2) < 0.00001 &
            .AND. ABS(F3) < 0.00001) EXIT
      END DO

      !..Calculate the wall shear stresses

      TAURT = C
      TAURZ = RJ2 * (1.0-LAMBDA2)

      RETURN

      END SUBROUTINE SIMPSONS


      SUBROUTINE INTERFACE (CC)
!***********************************************************************
!
!  PURPOSE:     Calculates the multiplier CC of VBAR required to match
!               the eroded radius at TREY=2100 for the turbulent and
!               laminar solutions for non-zero DOMEGA.  This process
!               accounts for drillstring rotation in the turbulent
!               flow regime.
!
!  AUTHOR:      Jerry W. Berglund
!
!  UPDATED:     xx-SEP-2004  --APG: Cosmetic changes;
!                              Isolated SIMPSONS into new routine;
!                              Moved some of the K=1,2 IFs
!               26-APR-1995  --JWB: Major changes to improve convergence.
!                                   Eliminated Newton's method for determining
!                                   TEFF.  Method for calculating seed for CC
!                                   changed.
!               29-MAR-1994  --JSR: Added write statements (w.r.t
!                                   "NEGATIVE ROUTER" computed)
!               17-FEB-1994  --JSR: Deleted DIAMMOD from COMMON /DRILL2/
!               28-OCT-1993  --JWB: Modified equation for the initial
!                                   selection of parameter CC.
!               31-AUG-1993  --JSR: Cosmetic changes
!
!  CALLED BY:   DRILL
!
!  ARGUMENTS:   None
!
!***********************************************************************

      USE CONSTANTS_MODULE
      USE DRILL_INTERNALS_MODULE
      USE DRILL_PARAMS_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION CC  !apg REAL

      INTEGER K, KK
      DOUBLE PRECISION COLD, &  !apg REAL
         D, DT2DR, &
         RDIF, ROLD, RORIG, &
         TAURT, TAURZ, TAU2, TAU2OLD, TEFF, &
         TFAIL, TREY, TREYOLD, VBAR, &
         VEL, TDIFOLD, TDIF, &
         CCINIT, ROUTERINIT

! Function definitions
      DOUBLE PRECISION FRICTION  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      !..Save ROUTER to restore at end
      RORIG = ROUTER

      !..Vary ROUTER and solve in laminar flow equations

      TAU2 = 0.0
      TREY = 0.0

      DO KK = 1, 1000
         ROUTER = RORIG + FLOAT(KK)*0.005

         !..Perform numerical integration of coefficients
         !..using Simpson's 1/3 rule
         !..Solve linear equations for corrections
         !..Calculate the wall shear stresses

         CALL SIMPSONS (TAURT, TAURZ)

         TAU2OLD = TAU2
         TAU2 = SQRT (TAURT*TAURT + TAURZ*TAURZ)

         TREYOLD = TREY
         VBAR = FLOWRATE/PI/(ROUTER**2-RINNER**2)  !!! OR flowrate / (pi*(...))
         TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
         IF (TREY <= 2100.) THEN
            TEFF = (TAU2OLD-TAU2)/(TREYOLD-TREY)*(2100.-TREY) + TAU2
            GOTO 100
         END IF
      END DO
  100 CONTINUE

      !..Turbulent flow regime

      TFAIL = TEFF

      !..Find a value of CC to use as a seed in the interation for the precise
      !..value of CC

      TDIF = 1E36
      DO KK = 1, 100
         CC = 0.20 + FLOAT(KK)*0.1

         !..Find Trey for chosen CC

  110    CONTINUE
         ROLD = ROUTER
         DO K = 1, 2
            IF (K == 2) ROUTER = 1.00001*ROUTER
            VBAR = FLOWRATE/(PI*(ROUTER**2-RINNER**2))
            TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
            TAU2OLD = TAU2
            VEL = CC*VBAR
            TAU2 = (0.5*FRICTION(TREY)*DENSITY*VEL**2/0.8165)**2
         END DO
         DT2DR = -(TAU2-TAU2OLD)/0.00001/ROUTER
         ROUTER = ROLD

         !..Calculate radius at which shear stress equals failure shear stress

         ROUTER = ROUTER + (TAU2OLD-TFAIL**2) / DT2DR
         IF (ROUTER <= RINNER) THEN
            ROUTER = RORIG
            CYCLE
         END IF
         RDIF = ROUTER-ROLD
         IF (ABS(RDIF) > 0.0001*RORIG) GOTO 110

         TDIFOLD = TDIF
         TDIF = ABS(TREY-2100.)
         IF (TDIF < TDIFOLD) THEN
            CCINIT = CC
            ROUTERINIT = ROUTER
         END IF
      END DO

      !..Iterate for the precise value of CC to generate TREY=2100.

      CC = CCINIT
      ROUTER = ROUTERINIT

  120 CONTINUE
      DO KK = 1, 2
         IF (KK == 2) CC = 1.001*CC

         !..Find Trey for assumed CC

  130    CONTINUE
         ROLD = ROUTER
         DO K = 1, 2
            IF (K == 2) ROUTER = 1.00001*ROUTER
            VBAR = FLOWRATE/(PI*(ROUTER**2-RINNER**2))
            TREY = 0.8165*4.0*DENSITY*VBAR*(ROUTER-RINNER)/ETA0
            TAU2OLD = TAU2
            VEL = CC*VBAR
            TAU2 = (0.5*FRICTION(TREY)*DENSITY*VEL**2/0.8165)**2
         END DO
         DT2DR = -(TAU2-TAU2OLD)/0.00001/ROUTER
         ROUTER = ROLD

         !..Calculate radius at which shear stress equals failure shear stress

         ROUTER = ROUTER + (TAU2OLD-TFAIL**2) / DT2DR
         RDIF = ROUTER-ROLD
         IF (ABS(RDIF) > 0.0001*RORIG) GOTO 130

         IF (KK == 1) COLD = CC
         IF (KK == 1) TREYOLD = TREY
      END DO
      D = (COLD-CC) / (TREYOLD-TREY)
      CC = CC - D*(TREY-2100.)

      IF (ABS(TREY-2100.) > 1.0) GOTO 120

      !..Restore ROUTER to original value
      ROUTER = RORIG

      RETURN

      END SUBROUTINE INTERFACE


      SUBROUTINE FXX (ALPHA, RHO, &
         F11, F12, F13, F21, F22, F23, F31, F32, F33, &
         EF1, EF2, EF3)
!***********************************************************************
!
!  PURPOSE:     Computes the numerical integration parameters for
!               Simpson's 1/3 rule.
!               A11,A12,A13, A21,A22,A23, A31,A32,A33 integrals.
!               F1, F3, F3 integrals.
!
!  AUTHOR:      Jerry W. Berglund
!
!  UPDATED:     xx-SEP-2004  --APG: Created by combining all Fxx and EFx
!                              routines into single routine
!
!  CALLED BY:   SIMPSONS
!
!  ARGUMENTS:
!     ALPHA     input  Ratio of inner and outer drillstring radii
!     RHO       input  Drilling mud density
!     F11       output A11 integral
!     F12       output A12 integral
!     F13       output A13 integral
!     F21       output A21 integral
!     F22       output A22 integral
!     F23       output A23 integral
!     F31       output A31 integral
!     F32       output A32 integral
!     F33       output A33 integral
!     EF1       output F1 integral
!     EF2       output F2 integral
!     EF3       output F3 integral
!
!***********************************************************************

      USE DRILL_INTERNALS_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION ALPHA, RHO  !apg REAL
      DOUBLE PRECISION F11, F12, F13, F21, F22, F23, F31, F32, F33  !apg REAL
      DOUBLE PRECISION EF1, EF2, EF3  !apg REAL

      DOUBLE PRECISION DIF, DIF2, E  !apg REAL
      DOUBLE PRECISION DERIV, DERIVL2, DERIVC, DERIVRJ2  !apg REAL

! Function definitions
      DOUBLE PRECISION ETA  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      DIF = RHO*RHO - LAMBDA2
      DIF2 = ALPHA*ALPHA - RHO*RHO

      E = ETA (RHO)

      EF1 = DIF/RHO/E
      EF2 = C/RHO**3/E
      EF3 = 4.*RJ2*DIF2/E*DIF/RHO

      DERIV = (SIGMA2*ETA0*.5 - SIGMA1*E*.5)**2 / ( &
         (SIGMA2*ETA0*.5 - SIGMA1*E*.5) * (E*E+(E-ETA0)*2.*E) &
         + (E-ETA0)*E*E*SIGMA1*.5 )

      DERIVL2  = -4.*RJ2*RJ2*DIF/RHO**2*DERIV
      DERIVC   =  4.*C/RHO**4*DERIV
      DERIVRJ2 =  4.*RJ2*DIF*DIF/(RHO*RHO)*DERIV

      F11 = -1./E/RHO*(1.+DIF/E*DERIVL2)
      F12 = -1./E/RHO*DIF/E*DERIVC
      F13 = -1./E/RHO*DIF/E*DERIVRJ2
      F21 = -C/E**2/RHO**3*DERIVL2
      F22 =  1./E/RHO**3*(1.-C/E*DERIVC)
      F23 = -C/E**2/RHO**3*DERIVRJ2
      F31 = -4.*RJ2*DIF2/E/RHO*(1.+DIF/E*DERIVL2)
      F32 = -4.*RJ2*DIF2/E/RHO*DIF/E*DERIVC
      F33 =  4.*DIF2*DIF/E/RHO-4.*RJ2*DIF2/E/RHO*DIF/E*DERIVRJ2

      RETURN

      END SUBROUTINE FXX


      DOUBLE PRECISION FUNCTION ETA (RHO)  !apg REAL
!***********************************************************************
!
!  PURPOSE:     Solves a cubic equation for the local value of
!               viscosity ETA.
!
!  AUTHOR:      Jerry W. Berglund
!
!  UPDATED:     xx-SEP-2004  --APG: Cosmetic changes
!               25-APR-1995  --JWB  Added coding for D < 0.
!               31-AUG-1993  --JSR: Cosmetic changes
!               23-AUG-1995  --RAC, JWB Fixed minor!! bug equation solver
!
!  CALLED BY:   DRILL
!
!  ARGUMENTS:
!     RHO       input  Drilling mud density
!     ETA       output
!
!***********************************************************************

      USE DRILL_INTERNALS_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION RHO  !apg REAL

      DOUBLE PRECISION P, Q, R, A, B, D, CAPA, CAPB, X, DIF, &  !apg REAL
         ARG1, ARG2, A1, A2, PHI

!**** EXECUTABLE STATEMENTS ****

      DIF = RHO*RHO - LAMBDA2
      P = -ETA0
      Q = SIGMA1 * (RJ2*RJ2*(DIF/RHO)**2 + C*C/RHO**4)
      R = -SIGMA2 * ETA0 * (RJ2*RJ2*(DIF/RHO)**2 + C*C/RHO**4)
      A = (3.*Q - P*P) / 3.
      B = (2.*P*P*P - 9.*P*Q + 27.*R) / 27.
      D = B*B*0.25 + A*A*A / 27.
      IF (D < 0.) THEN
         IF (B < 0.) PHI = ACOS(SQRT(B*B/4./(-A*A*A/27.)))
         IF (B > 0.) PHI = ACOS(-SQRT(B*B/4./(-A*A*A/27.)))
         X = 2. * SQRT(-A/3.) * COS(PHI/3.)
      ELSE
         ARG1 = -B*0.5 + SQRT(D)
         ARG2 = -B*0.5 - SQRT(D)
         A1 = ARG1
         A2 = ARG2
         IF (ARG1 < 0.) A1 = -A1
         IF (ARG2 < 0.) A2 = -A2
         CAPA = A1**.33333333333333333333
         CAPB = A2**.33333333333333333333
         IF (ARG1 < 0.) CAPA = -CAPA
         IF (ARG2 < 0.) CAPB = -CAPB
         X = CAPA+CAPB
      END IF
      ETA = X-P/3.

      RETURN

      END FUNCTION ETA


      DOUBLE PRECISION FUNCTION FRICTION (TREY)  !apg REAL
!***********************************************************************
!
!  PURPOSE:     Calculates Fanning friction factor when flow is turbulent
!
!  AUTHOR:      Jerry W. Berglund
!
!  UPDATED:     xx-SEP-2004  --APG: Cosmetic changes
!               26-APR-1995  --JWB: Tightened convergence criterion for G
!               17-FEB-1994  --JSR: Deleted DIAMMOD from COMMON /DRILL2/
!               31-AUG-1993  --JSR: Cosmetic changes
!
!  CALLED BY:   DRILL
!
!  ARGUMENTS:
!     TREY      input  Turbulent flow Reynold's number
!     FRICTION  output
!
!***********************************************************************

      USE DRILL_PARAMS_MODULE

      IMPLICIT NONE

      DOUBLE PRECISION TREY  !apg REAL

      DOUBLE PRECISION C1, C2, DERIV, G, X  !apg REAL

!**** EXECUTABLE STATEMENTS ****

      C1    = ABSRO/3.72/((ROUTER-RINNER)*2.0)
      C2    = 1.255/TREY
      X     = 50.0
      DO WHILE (.TRUE.)
         DERIV = 1.0 + 4.0*C2*0.434294482/(C1+C2*X)
         G     = X + 4.0*LOG10(C1+C2*X)
         X     = X - G/DERIV
         IF (ABS(G) < 0.00001) EXIT
      END DO
      FRICTION = 1.0/(X*X)

      RETURN

      END FUNCTION FRICTION
